<?php

namespace backend\controllers;
use frontend\models\Adjudicator;
use frontend\models\Team;
use frontend\models\Round;
use frontend\models\AttendanceAdj;
use frontend\models\AttendanceTeam;

class AttendanceController extends \yii\web\Controller
{
    public function actionAdj()
    {
    	$adjs = Adjudicator::find()->all();
    	$rounds = Round::find()->all();
    	$att = [];

    	foreach($adjs as $a=>$adj){
    		$att[$a]['id'] = $adj->id;
    		$att[$a]['name'] = $adj->name;
    		$att[$a]['institution'] = $adj->institution;

    		foreach($rounds as $r=>$round){
    			$attendance = AttendanceAdj::find()->where(['adjudicator_id'=>$adj->id, 'round'=>$round->round])->one();
    			if($attendance == null) $att[$a]['r'.$round->round] = '';
    			else $att[$a]['r'.$round->round] = $attendance->check_in;
    		}
    	}

        return $this->render('adj', ['att'=>$att]);
    }

    public function actionTeam()
    {
    	$teams = Team::find()->all();
    	$rounds = Round::find()->all();
    	$att = [];

    	foreach($teams as $a=>$team){
    		$att[$a]['id'] = $team->id;
    		$att[$a]['team_name'] = $team->team_name;

    		foreach($rounds as $r=>$round){
    			$attendance = AttendanceTeam::find()->where(['team_id'=>$team->id, 'round'=>$round->round])->one();
    			if($attendance == null) $att[$a]['r'.$round->round] = '';
    			else $att[$a]['r'.$round->round] = $attendance->check_in;
    		}
    	}
        return $this->render('team', ['att'=>$att]);
    }

}
