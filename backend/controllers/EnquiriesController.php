<?php

namespace backend\controllers;

use Yii;
use frontend\models\Enquiries;
use frontend\models\Feedback;
use frontend\models\FeedbackDetail;
use frontend\models\FeedbackTag;
use backend\models\EnquiriesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


/**
 * EnquiriesController implements the CRUD actions for Enquiries model.
 */
class EnquiriesController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Enquiries models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EnquiriesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Enquiries model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Enquiries model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Enquiries();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Enquiries model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionReject($id){
        $model = $this->findModel($id);
        $model->status = 'r';
        $model->save(false);
        return $this->redirect('index');
    }

    public function actionApprove($id){
        $model = $this->findModel($id);

        $feedback = Feedback::find()->where([
            'round' => $model->round,
            'adj_id' => $model->adj_id,
            'giver_adj_id' => $model->giver_adj_id,
            'giver_team_id' => $model->giver_team_id,
        ])->one();
        if($feedback == null){
            $feedback = new Feedback();
            $feedback->attributes = $model->attributes;
            $feedback->status = 'e';

            $model->status = 'a';
            $feedback->save(false);

            foreach($model->enquiriesDetails as $ed){
                $fd = new FeedbackDetail();
                $fd->attributes = $ed->attributes;
                $fd->feedback_id = $feedback->id;
                $fd->save(false);
            }
            foreach($model->enquiriesTags as $et){
                $ft = new FeedbackTag();
                $ft->attributes = $et->attributes;
                $ft->feedback_id = $feedback->id;
                $ft->save(false);
            }
            $model->save(false);
        }
        else{
            $model->status = 'c';
            $model->save(false);
        }

        return $this->redirect('index');
    }

    public function actionOpen($id){
        $model = $this->findModel($id);
        $model->status = 'o';
        $model->save(false);
        return $this->redirect('index');
    }

    /**
     * Deletes an existing Enquiries model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Enquiries model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Enquiries the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Enquiries::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
