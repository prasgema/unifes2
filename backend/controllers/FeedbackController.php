<?php

namespace backend\controllers;

use Yii;
use frontend\models\Feedback;
use frontend\models\FeedbackDetail;
use frontend\models\Adjudicator;
use frontend\models\Round;
use backend\models\FeedbackSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use kartik\helpers\Html;

/**
 * FeedbackController implements the CRUD actions for Feedback model.
 */
class FeedbackController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Feedback models.
     * @return mixed
     */
    public function actionIndex($marking = 'on')
    {
        $adjs = Adjudicator::find()->all();
        $rounds = Round::find()->all();
        $list = array();
        $score = array();
        $count = array();
        $query = new Query();
        $config = Yii::$app->params['config'];
        $lastround = 0;
        
        foreach($adjs as $a){
            $list[$a->id] = [
                'id' => $a->id,
                'name' => $a->name,
                'institution' => $a->institution,
                'test' => $a->initial_score
            ];
        }

        $result = $query->select('*')->from('adj_feedback')->where('score > 0')->all();

        foreach($result as $r){
            $round_score = (100/$config['max_score'])*round($r['score'],3);
            $list[$r['id']]['R'.$r['round']] = $round_score;
            
            if(!isset($score[$r['id']])){
                $oldavg = $list[$r['id']]['test'];
                $score[$r['id']] = $round_score;
                $count[$r['id']] = 1;
            } else {
                $oldavg = ($list[$r['id']]['test']+$score[$r['id']])/($count[$r['id']]+1);
                $score[$r['id']] += $round_score;
                $count[$r['id']] += 1;
            }
            if($lastround < $r['round']) $lastround = $r['round'];

            if($marking == 'on'){
                if($round_score >= $oldavg+25) $list[$r['id']]['R'.$r['round']] = Html::bsLabel($round_score, Html::TYPE_SUCCESS);
                else if($round_score >= $oldavg+15) $list[$r['id']]['R'.$r['round']] = Html::bsLabel($round_score, Html::TYPE_INFO);
                else if($round_score <= $oldavg-15) $list[$r['id']]['R'.$r['round']] = Html::bsLabel($round_score, Html::TYPE_WARNING);
                else if($round_score <= $oldavg-25) $list[$r['id']]['R'.$r['round']] = Html::bsLabel($round_score, Html::TYPE_DANGER);
            } else { $list[$r['id']]['R'.$r['round']] = $round_score; }
        }
        foreach($list as $i=>$l){
            if(isset($count[$i]) && $count[$i]>0) $list[$i]['score'] = round( ($score[$i]+((count($rounds)+1-$lastround)*$list[$i]['test']))/(count($rounds)+1), 2);
            else $list[$i]['score'] = $list[$i]['test'];
        }

        return $this->render('index', ['list' => $list, 'rounds'=>$rounds]);
    }

    public function actionWatch($id)
    {
        $model = Adjudicator::findOne($id);
        $query = new Query();

        $dataProvider = new ActiveDataProvider([
            'query' => Feedback::find()->where(['adj_id'=>$id])->orderBy('round'),
            'pagination' => false
        ]);

        return $this->render('watch', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Feedback models.
     * @return mixed
     */
    public function actionBrowse()
    {
        $searchModel = new FeedbackSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('browse', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Feedback model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Feedback model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Feedback();
        $d_mdl = new FeedbackDetail();

        if ($model->load(Yii::$app->request->post())) {
            $model->status = 'a';
            if($model->save()){
                foreach($question as $j => $q){
                    $d_mdl = new FeedbackDetail();
                    $d_mdl->feedback_id = $model->id;
                    $d_mdl->question_id = $q->id;
                    $d_mdl->score = $_POST['FeedbackDetail'][$i][$j]['score'];

                    $d_mdl->save();
                } //foreach question

                foreach($model->tag as $j => $t){
                    $t_mdl = new FeedbackTag();
                    $t_mdl->feedback_id = $model->id;
                    $t_mdl->tag_id = $t;

                    $t_mdl->save();
                } //foreach tag

                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'd_mdl' => $d_mdl,
            ]);
        }
    }

    /**
     * Updates an existing Feedback model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $d_mdl = $model->feedbackDetails;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            foreach($question as $j => $q){
                $d_mdl = new FeedbackDetail();
                $d_mdl->feedback_id = $model->id;
                $d_mdl->question_id = $q->id;
                $d_mdl->score = $_POST['FeedbackDetail'][$i][$j]['score'];

                $d_mdl->save();
            } //foreach question

            foreach($model->tag as $j => $t){
                $t_mdl = new FeedbackTag();
                $t_mdl->feedback_id = $model->id;
                $t_mdl->tag_id = $t;

                $t_mdl->save();
            } //foreach tag

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'd_mdl' => $d_mdl,
            ]);
        }
    }

    /**
     * Deletes an existing Feedback model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        

        return $this->redirect(['index']);
    }

    /**
     * Finds the Feedback model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Feedback the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Feedback::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
