<?php

namespace backend\controllers;
use Yii;
use frontend\models\Team;
use frontend\models\LanguageStatus;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;

class LanguagestatusController extends \yii\web\Controller
{
    public function actionApprove($id, $status, $speaker)
    {
        $model = $this->findModel($id)->languageStatus;

        switch($speaker){
            case 1 : $model->speaker_1_status = $status; break;
            case 2 : $model->speaker_2_status = $status; break;
            case 3 : $model->speaker_3_status = $status; break;
        }

        $model->save();

        return $this->redirect('view', $id);
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Team::find()->orderBy('team_name')->where('id IN (SELECT team_id FROM language_status)'),
            'pagination' => false,
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $questions = explode(';', Yii::$app->params['config']['language_status_interview_questions']);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'questions' => $questions,
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Team::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
