<?php

namespace backend\controllers;

use Yii;
use frontend\models\Round;
use frontend\models\Debate;
use frontend\models\DebateAdj;
use frontend\models\Adjudicator;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * RoundController implements the CRUD actions for Round model.
 */
class RoundController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Round models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Round::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Round model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $debate = Debate::find();
        $adj = new ActiveDataProvider([
            'query' => DebateAdj::find()->leftJoin(['debate'=>$debate], 'debate.id = debate_id')->where(['round'=>$id])->orderBy('room, status'),
            'pagination'=>false,
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'adj' => $adj,
        ]);
    }

    public function actionPending($id){
        $model = $this->findModel($id);
        $model->status = 'p';
        $model->save();
        $this->redirect('index');
    }

    public function actionOpen($id){
        $model = $this->findModel($id);
        $model->status = 'o';
        $model->save();
        $this->redirect('index');
    }

    public function actionSilent($id){
        $model = $this->findModel($id);
        $model->status = 's';
        $model->save();
        $this->redirect('index');
    }

    public function actionImport($round){
        $arr = Round::import($round);
        foreach($arr['debate'] as $r){
            $model = Debate::findOne($r['id']);
            if($model == null) $model = new Debate();
            $model->id = $r['id'];
            $model->round = $r['round'];
            $model->room = $r['room'];
            $model->team_id_og = $r['team_id_og'];
            $model->team_id_oo = $r['team_id_oo'];
            $model->team_id_cg = $r['team_id_cg'];
            $model->team_id_co = $r['team_id_co'];
            
            $model->save(false);

            foreach($model->debateAdj as $a) $a->delete();
        }
        foreach($arr['debateAdj'] as $r){
            $model = new DebateAdj();
            $model->debate_id = $r['debate_id'];
            $model->adj_id = $r['adj_id'];
            $model->status = $r['status'];

            $model->save(false);
        }
        $this->redirect('index');
    }

    public function actionUpdatealloc($adj_id, $debate_id){
        $model = DebateAdj::find()->where(['adj_id'=>$adj_id, 'debate_id'=>$debate_id])->one();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->debate->round]);
        } else {
            $adj = Adjudicator::find()->all();
            $debate = Debate::find()->where(['round'=>$model->debate->round])->all();
            return $this->render('updatealloc', [
                'model' => $model,
                'adj' => $adj,
                'debate' => $debate,
            ]);
        }
    }

    /**
     * Creates a new Round model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Round();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->round]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Round model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->round]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Round model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Round model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Round the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Round::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
