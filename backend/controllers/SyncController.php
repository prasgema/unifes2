<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\db\Query;
use yii\helpers\Url;
use frontend\models\Feedback;
use frontend\models\Debate;
use frontend\models\Adjudicator;
use frontend\models\Team;
use frontend\models\Round;

class SyncController extends Controller
{
    public $enableCsrfValidation = false;
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'get', 'import'],
                        'allow' => true,
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'import' => ['post'],
                ],
            ],
        ];
    }

    private function serve($what, $site){
        // Get cURL resource
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $site.'/sync/get?what='.$what,
        ]);
        // Send the request & save response to $resp
        $result = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);

        return unserialize($result);
    }

    private function push($what, $data, $site){
        $kill = Yii::$app->params['config']['kill_switch'];
        // Get cURL resource
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $site.'/sync/import',
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => [
                'import',
                'data' => serialize($data),
                'what' => $what,
                'killswitch' => $kill,
            ]
        ]);
        // Send the request & save response to $resp
        $result = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);

        return $result;
    }

    private function localData($what){
        return $this->serve($what, Url::home(true));
    }

    private function onlineData($what){
        return $this->serve($what, Yii::$app->params['config']['online'].'/backend');
    }

    public function actionExecute($what, $ft){
        switch($ft){
            case 'o2l' :
                $online = $this->onlineData($what);
                $result = $this->push($what, $online, Url::home(true));
            break;

            case 'l2o' :
                $local = $this->localData($what);
                $result = $this->push($what, $local, Yii::$app->params['config']['online'].'/backend');
            break;
        }
        return $result;

        //return $this->redirect(['sync/index']);
    }

    public function actionAdjudicator()
    {
        return $this->render('data', [
            'online' => $this->onlineData('adjudicator'),
            'local' => $this->localData('adjudicator'),
        ]);
    }

    public function actionRound()
    {
        return $this->render('data', [
            'online' => $this->onlineData('round'),
            'local' => $this->localData('round'),
        ]);
    }

    public function actionTeam()
    {
        return $this->render('data', [
            'online' => $this->onlineData('team'),
            'local' => $this->localData('team'),
        ]);
    }

    public function actionDebate()
    {
        return $this->render('data', [
            'online' => $this->onlineData('debate'),
            'local' => $this->localData('debate'),
        ]);
    }

    public function actionDebateadjudicator()
    {
        return $this->render('data', [
            'online' => $this->onlineData('debateadjudicator'),
            'local' => $this->localData('debateadjudicator'),
        ]);
    }

    public function actionFeedback()
    {
        return $this->render('data', [
            'online' => $this->onlineData('feedback'),
            'local' => $this->localData('feedback'),
        ]);
    }

    public function actionFeedbackcheck()
    {
        return $this->render('feedbackcheck', [
            'online' => $this->onlineData('feedback'),
            'local' => $this->localData('feedback'),
        ]);
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionGet($what = null)
    {
        $query = new Query();

        switch($what){
            case 'debate' : 
                $result = $query->select('*')->from('debate')->all();
                break;
            case 'debateadjudicator' : 
                $result = $query->select('*')->from('debate_adj')->all();
                break;
            case 'feedback' :
                $result = new Feedback;
                $result = $result->populate();
                break;
            case 'team' :
            case 'adjudicator' :
            case 'round' :
                $result = $query->select('*')->from($what)->all();
                break;
            default :
                return 'KISS MY GLOSSY ALUMINIUM ASS!';
                break;
        }

        return serialize($result);
    }

    public function actionImport(){
        $kill = Yii::$app->params['config']['kill_switch'];
        $what = $_POST['what'];
        $killswitch = $_POST['killswitch'];
        $data = unserialize( $_POST['data'] );
        
        if($killswitch == $kill){
            switch($what){
                case 'debate' : 
                    foreach($data as $d){
                        Debate::deleteAll();
                        $debate = new Debate;
                        $debate->attributes = $d;
                        $debate->save(false);
                    }
                    return 'OK';
                break;

                case 'debateadjudicator' : 
                    foreach($data as $d){
                        DebateAdj::deleteAll();
                        $adj = new DebateAdj;
                        $adj->attributes = $d;
                        $adj->save(false);
                    }
                    return 'OK';
                break;

                case 'feedback' :
                    $question = Question::find()->all();
                    foreach($data as $d){
                        $feedback = Feedback::find()->where([
                            'round' => $d['round'],
                            'adj_id' => $d['adj_id'],
                            'giver_team_id' => $d['giver_team_id'],
                            'giver_adj_id' => $d['giver_adj_id'],
                        ])->one();

                        if($feedback == null){
                            $feedback = new Feedback;
                            $feedback->round = $d['round'];
                            $feedback->adj_id = $d['adj_id'];
                            $feedback->giver_team_id = $d['giver_team_id'];
                            $feedback->giver_adj_id = $d['giver_adj_id'];
                            $feedback->giver_status = $d['giver_status'];
                            $feedback->agree_initial = $d['agree_initial'];
                            $feedback->agree_eventual = $d['agree_eventual'];
                            $feedback->influence = $d['influence'];
                            $feedback->comments = $d['comments'];
                            $feedback->status = $d['status'];
                            $feedback->created_at = $d['created_at'];

                            $feedback->save(false);
                            $scores = explode(',',$d['detail']);
                            $tags = explode(',',$d['tag']);

                            foreach($question as $j => $q){
                                $detail = new FeedbackDetail();
                                $detail->feedback_id = $feedback->id;
                                $detail->question_id = $q->id;
                                $detail->score = $scores[$j];

                                $detail->save(false);
                            }

                            foreach($tags as $j => $t){
                                $tag = new FeedbackTag();
                                $tag->feedback_id = $feedback->id;
                                $tag->tag_id = $t;

                                $tag->save(false);
                            }
                        }
                    }
                break;

                case 'adjudicator' : 
                    foreach($data as $d){
                        $adj = Adjudicator::findOne($d['id']);
                        if($adj == null) $adj = new Adjudicator;
                        $adj->attributes = $d;
                        $adj->save(false);
                    }
                    return 'OK';
                break;

                case 'team' :
                    foreach($data as $d){
                        $team = Team::findOne($d['id']);
                        if($team == null) $team = new Team;
                        $team->attributes = $d;
                        $team->save(false);
                    }
                    return 'OK';
                break;

                case 'round' :
                    foreach($data as $d){
                        $round = Round::findOne($d['round']);
                        if($round == null) $round = new Round;
                        $round->attributes = $d;
                        $round->save(false);
                    }
                    return 'OK';
                break;

                default :
                    return 'KISS MY GLOSSY ALUMINIUM ASS!';
                break;
            }
        }
    }

    public function actionCommitfeedback(){
        //only for tabbycat
        //if(Yii::$app->params['config']['system'] != 'tabbycat') return 'METHOD NOT ALLOWED, THIS IS NOT A TABBYCAT TOURNEY';

        $feedbacks = Feedback::find()->all();
        $connection = \Yii::$app->tabbycat;
        $query = '';

        foreach($feedbacks as $f){
            $date = date('Y-m-d H:i:s');
            $comments = addslashes($f->comments);
            $giver_team_id = $f->giver_team_id;

            $sourceteam = $connection->createCommand("
                SELECT DT.id AS id
                FROM debate_debateteam DT JOIN debate_debate D ON D.id = DT.debate_id
                WHERE DT.team_id = T.id AND T.institution_id = I.id AND 
                AND round_id = 5 AND DT.team_id = {$giver_team_id}
                ORDER BY CONCAT(I.code,' ',T.reference)
            ")->queryOne();

            $sourceteam = $sourceteam['id'];

            /*$query = $connection->createCommand("
                INSERT INTO debate_adjudicatorfeedback (
                    timestamp, version, submitter_type,
                    confirmed, adjudicator_id, score,
                    agree_with_decision, comments, source_team_id
                ) VALUES (
                    '{$date}', 1, 0,
                    TRUE, 181, 5,
                    TRUE, '{$comments}', {$sourceteam}
                )
            ");
            $query->execute();*/
            $query.="
                INSERT INTO debate_adjudicatorfeedback (
                    timestamp, version, submitter_type,
                    confirmed, adjudicator_id, score,
                    agree_with_decision, comments, source_team_id
                ) VALUES (
                    '{$date}', 1, 0,
                    TRUE, 181, 5,
                    TRUE, '{$comments}', {$sourceteam}
                )\n
            ";
        }
        
        return $query;
    }
}
