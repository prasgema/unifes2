<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "config".
 *
 * @property integer $id
 * @property string $unique_code
 * @property string $tagging
 * @property string $chair_to
 * @property string $panel_to
 * @property string $trainee_to
 * @property string $team_to
 * @property integer $max_score
 */
class Config extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'unique_code', 'tagging', 'chair_to', 'panel_to', 'trainee_to', 'team_to', 'max_score', 'system', 'online'], 'required'],
            [['system','online'], 'string'],
            [['id', 'max_score'], 'integer'],
            [['unique_code', 'tagging', 'chair_to', 'panel_to', 'trainee_to', 'team_to'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'unique_code' => 'Unique Code',
            'tagging' => 'Tagging',
            'chair_to' => 'Chair To',
            'panel_to' => 'Panel To',
            'trainee_to' => 'Trainee To',
            'team_to' => 'Team To',
            'max_score' => 'Max Score',
            'system' => 'System',
            'online' => 'Address to Online System',
        ];
    }
}
