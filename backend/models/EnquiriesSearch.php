<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Enquiries;
use frontend\models\Adjudicator;
use frontend\models\Team;

/**
 * EnquiriesSearch represents the model behind the search form about `frontend\models\Enquiries`.
 */
class EnquiriesSearch extends Enquiries
{
    public $adjName;
    public $giverTeamName;
    public $giverAdjName;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'round', 'adj_id', 'giver_team_id', 'giver_adj_id'], 'integer'],
            [['adjName','giverAdjName','giverTeamName','status'], 'string'],
            [['adjName','giverAdjName','giverTeamName','room', 'giver_status', 'agree_initial', 'agree_eventual', 'influence', 'comments', 'status', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Enquiries::find();
        $adj = Adjudicator::find();
        $team = Team::find();

        $query->leftJoin(['adj'=>$adj], 'adj.id = adj_id')
            ->leftJoin(['giver_adj'=>$adj], 'giver_adj.id = giver_adj_id')
            ->leftJoin(['giver_team'=>$team], 'giver_team.id = giver_team_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'status',
                'round',
                'room',
                'adjName' => [
                    'asc' => ['adj.name' => SORT_ASC],
                    'desc' => ['adj.name' => SORT_DESC],
                    'label' => 'Adjudicator'
                ],
                'giverAdjName' => [
                    'asc' => ['giver_adj.name' => SORT_ASC],
                    'desc' => ['giver_adj.name' => SORT_DESC],
                    'label' => 'Giver Adj'
                ],
                'giverTeamName' => [
                    'asc' => ['giver_team.team_name' => SORT_ASC],
                    'desc' => ['giver_team.team_name' => SORT_DESC],
                    'label' => 'Team'
                ],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider->where(['status'=>'o']);
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'round' => $this->round,
            'status' => $this->status,
            'adj_id' => $this->adj_id,
            'giver_team_id' => $this->giver_team_id,
            'giver_adj_id' => $this->giver_adj_id,
            'created_at' => $this->created_at,
        ]);

        if($this->adjName) $query->andWhere("adj.name LIKE '%{$this->adjName}%'");
        if($this->giverAdjName) $query->andWhere("giver_adj.name LIKE '%{$this->giverAdjName}%'");
        if($this->giverTeamName) $query->andWhere("giver_team.team_name LIKE '%{$this->giverTeamName}%'");

        return $dataProvider;
    }
}
