<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Adjudicator */

$this->title = 'Create Adjudicator';
$this->params['breadcrumbs'][] = ['label' => 'Adjudicators', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adjudicator-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
