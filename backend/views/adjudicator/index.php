<?php

use kartik\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\ButtonGroup;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\AdjudicatorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Adjudicators';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adjudicator-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= ButtonGroup::widget([
            'buttons' => [
                Html::a('Create Adjudicator', ['create'], ['class' => 'btn btn-primary']),
                Html::a('Import Debate Data', ['import'], ['class' => 'btn btn-success']),
            ]
        ]) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'institution',
            'initial_score',
            'unique_code',
            [
                'label'=>'Status',
                'format'=>'raw',
                'value'=> function($model){
                    if($model->status == 'o') return Html::a(Html::bsLabel('OPEN', Html::TYPE_SUCCESS), ['adjudicator/status', 'id'=>$model->id]);
                    else if($model->status == 'l') return Html::a(Html::bsLabel('LOCK', Html::TYPE_DANGER), ['adjudicator/status', 'id'=>$model->id]);
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
