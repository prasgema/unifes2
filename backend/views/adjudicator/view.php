<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Adjudicator */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Adjudicators', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adjudicator-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'institution',
            'initial_score',
            'unique_code',
            'status',
        ],
    ]) ?>

</div>
