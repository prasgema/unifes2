<?php
use yii\data\ArrayDataProvider;
use yii\grid\GridView;

/* @var $this yii\web\View */

$columns = array_keys($att[0]);

$att = new ArrayDataProvider([
    'allModels' => $att,
    'pagination' => false
]);
?>
<h1>attendance/adj</h1>

<div class="col-lg-12">
    <?= GridView::widget([
        'dataProvider' => $att,
        'emptyCell'=>'',
        'columns' => $columns,
        'summary'=>false,
    ]) ?>
</div>

