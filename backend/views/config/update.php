<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Config */

$this->title = 'Update Config';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="config-update">

    <h1><?= Html::encode($this->title) ?></h1>


	<div class="config-form">

	    <?php $form = ActiveForm::begin(); ?>

	    <?= $form->field($model, 'unique_code')->dropdownList(['y' => 'Yes', 'n'=>'No']) ?>

	    <?= $form->field($model, 'tagging')->dropdownList(['y' => 'Yes', 'n'=>'No']) ?>

	    <?= $form->field($model, 'comment')->dropdownList(['y' => 'Yes', 'n'=>'No']) ?>

	    <?= $form->field($model, 'chair_to')->dropdownList(['*' => 'All Adjes (Except Him/erself)', 'p'=>'Panelists & Trainees', '?'=>'No one']) ?>

	    <?= $form->field($model, 'panel_to')->dropdownList(['*' => 'All Adjes (Except Him/erself)', 'c'=>'Chair Adj', 'p'=>'Panelists & Trainees', '?'=>'No one']) ?>

	    <?= $form->field($model, 'trainee_to')->dropdownList(['*' => 'All Adjes (Except Him/erself)', 'c'=>'Chair Adj', 'p'=>'Panelists & Trainees', '?'=>'No one']) ?>

	    <?= $form->field($model, 'team_to')->dropdownList(['*' => 'All Adjes', 'c'=>'Chair Adj', '?'=>'No one']) ?>
		
		<?= $form->field($model, 'adjfeedback_on_silent')->dropdownList(['y' => 'Yes', 'n'=>'No']) ?>

	    <?= $form->field($model, 'max_score')->textInput() ?>

	    <?= $form->field($model, 'system')->dropdownList(['tabbie' => 'Tabbie', 'tabbycat'=>'Tabbycat']) ?>

	    <?= $form->field($model, 'online')->textInput() ?>

	    <?= $form->field($model, 'kill_switch')->textInput() ?>

	    <?= $form->field($model, 'language_status')->dropdownList(['esl,efl'=>'esl,efl', 'efl'=>'efl', 'esl'=>'esl', 'novice'=>'novice']) ?>

	    <?= $form->field($model, 'language_status_interview_questions')->textArea() ?>

	    <div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>

	    <?php ActiveForm::end(); ?>

	</div>

</div>
