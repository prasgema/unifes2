<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use frontend\models\Adjudicator;
use frontend\models\Team;
use frontend\models\Round;

/* @var $this yii\web\View */
/* @var $model frontend\models\Enquiries */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="enquiries-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'round')->dropdownList(ArrayHelper::map(Round::find()->all(), 'round', 'round')) ?>

    <?= $form->field($model, 'room')->textInput(['maxlength' => 8]) ?>

    <?= $form->field($model, 'adj_id')->widget(Select2::classname(),[
        'data' => ArrayHelper::map(Adjudicator::find()->all(), 'id', 'nameInst')
    ]) ?>

    <?= $form->field($model, 'giver_team_id')->widget(Select2::classname(),[
        'data' => ArrayHelper::map(Team::find()->all(), 'id', 'team_name')
    ]) ?>

    <?= $form->field($model, 'giver_adj_id')->widget(Select2::classname(),[
        'data' => ArrayHelper::map(Adjudicator::find()->all(), 'id', 'nameInst')
    ]) ?>

    <?= $form->field($model, 'giver_status')->dropdownList(['c' => 'Chair', 'p'=>'Panelist', 't'=>'Trainee', 'og'=>'Opening Government', 'oo'=>'Opening Opposition', 'cg'=>'Closing Government', 'co'=>'Closing Opposition']) ?>

    <?= $form->field($model, 'agree_initial')->dropdownList(['y'=>'Yes', 'n'=>'No']) ?>

    <?= $form->field($model, 'agree_eventual')->dropdownList(['y'=>'Yes', 'n'=>'No']) ?>

    <?= $form->field($model, 'influence')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'comments')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->dropdownList(['n'=>'Normal', 'e'=>'Enquiries', 'a'=>'Admin Page']) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?php 
        $questions = Question::find()->all();
        if(!$model->isNewRecord) foreach($questions as $i => $q) echo $form->field($d_mdl[$i], "score")->dropdownList(array_combine(range(1,$config['max_score']),range(1,$config['max_score'])));
        else foreach($questions as $i => $q) echo $form->field($d_mdl, "[{$i}]score")->dropdownList(array_combine(range(1,$config['max_score']),range(1,$config['max_score'])));
    ?>
    <?= $form->field($model, "tag")->checkboxList(ArrayHelper::map(Tag::find()->all(), 'id', 'tag'), ['separator'=>'&emsp;&emsp;', 'itemOptions' => ['class' =>'radio-inline', 'style'=>'margin:0 5px;']]) ?>

    <?php 
        $questions = Question::find()->all();
        foreach($questions as $i => $q) echo $form->field($d_mdl, "[{$i}]created_at")->dropdownList(array_combine(range(1,$config['max_score']),range(1,$config['max_score'])));
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
