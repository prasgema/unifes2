<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Enquiries */

$this->title = 'Create Enquiries';
$this->params['breadcrumbs'][] = ['label' => 'Enquiries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="enquiries-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
