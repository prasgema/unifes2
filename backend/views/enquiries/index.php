<?php

use kartik\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\EnquiriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Enquiries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="enquiries-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Enquiries', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute'=>'status',
                'format'=>'raw',
                'value'=>function($model){
                    return $model->status=='o' ? Html::bsLabel('Waiting') : 
                        ($model->status=='r' ? Html::bsLabel('Rejected', Html::TYPE_DANGER) : 
                            ($model->status=='c' ? Html::bsLabel('Duplicate Found', Html::TYPE_WARNING) : 
                                Html::bsLabel('Approved', Html::TYPE_SUCCESS)));
                }
            ],
            'round',
            'room',
            'giverAdjName',
            'giverTeamName',
            'adjName',
            'averageScore',
            // 'giver_adj_id',
            // 'giver_status',
            // 'agree_initial',
            // 'agree_eventual',
            // 'influence',
            // 'comments:ntext',
            // 'status',
            // 'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
