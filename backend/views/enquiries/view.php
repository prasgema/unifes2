<?php

use kartik\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Enquiries */

$this->title = 'Enquiry #'.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Enquiries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="enquiries-view">

    <h1>[R<?= $model->round ?>] <?= $model->giver ?> > <?= $model->adj->name ?></h1>
    <p><?= $model->status=='o' ? Html::bsLabel('Waiting') : ($model->status=='r' ? Html::bsLabel('Rejected', Html::TYPE_DANGER) : ($model->status=='c' ? Html::bsLabel('Duplicate Found', Html::TYPE_WARNING) : Html::bsLabel('Approved', Html::TYPE_SUCCESS))) ?></p>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= $model->status != 'o' ? Html::a('Revoke', ['open', 'id' => $model->id], ['class' => 'btn btn-warning']) : '' ?>
        <?= $model->status != 'a' ? Html::a('Approve', ['approve', 'id' => $model->id], ['class' => 'btn btn-success']) : '' ?>
        <?= $model->status != 'r' ? Html::a('Reject', ['reject', 'id' => $model->id], ['class' => 'btn btn-danger']) : '' ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'round',
            'room',
            'adj.name:raw:Adj Name',
            'giver',
            'giverStatus',
            'agree:raw',
            'feedbackInfluence:raw:feedbackInfluence',
            'averageScore',
            'scores:raw',
            'comments:ntext',
            'created_at',
        ],
    ]) ?>

</div>
