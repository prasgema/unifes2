<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use frontend\models\Adjudicator;
use frontend\models\Team;
use frontend\models\Round;
use frontend\models\Question;
use frontend\models\Tag;

/* @var $this yii\web\View */
/* @var $model frontend\models\Feedback */
/* @var $form yii\widgets\ActiveForm */

$config = Yii::$app->params['config'];
?>

<div class="feedback-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'round')->dropdownList(ArrayHelper::map(Round::find()->all(), 'round', 'round')) ?>

    <?= $form->field($model, 'adj_id')->widget(Select2::classname(),[
        'data' => ArrayHelper::map(Adjudicator::find()->all(), 'id', 'nameInst'),
        'options' => ['prompt'=>' - PILIH - '],
        'pluginOptions' => ['allowClear'=> true],
    ]) ?>

    <?= $form->field($model, 'giver_team_id')->widget(Select2::classname(),[
        'data' => ArrayHelper::map(Team::find()->all(), 'id', 'team_name'),
        'options' => ['prompt'=>' - PILIH - '],
        'pluginOptions' => ['allowClear'=> true],
    ]) ?>

    <?= $form->field($model, 'giver_adj_id')->widget(Select2::classname(),[
        'data' => ArrayHelper::map(Adjudicator::find()->all(), 'id', 'nameInst'),
        'options' => ['prompt'=>' - PILIH - '],
        'pluginOptions' => ['allowClear'=> true],
    ]) ?>

    <?= $form->field($model, 'giver_status')->dropdownList(['c' => 'Chair', 'p'=>'Panelist', 't'=>'Trainee', 'og'=>'Opening Government', 'oo'=>'Opening Opposition', 'cg'=>'Closing Government', 'co'=>'Closing Opposition']) ?>

    <?= $form->field($model, 'agree_initial')->dropdownList(['y'=>'Yes', 'n'=>'No'], ['prompt'=>' - PILIH - ']) ?>

    <?= $form->field($model, 'agree_eventual')->dropdownList(['y'=>'Yes', 'n'=>'No']) ?>

    <?= $form->field($model, 'influence')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'comments')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->dropdownList(['n'=>'Normal', 'e'=>'Enquiries', 'a'=>'Admin Page']) ?>

    <?php if($model->isNewRecord) $model->created_at = date('Y-m-d H:i:s'); ?>
    <?= $form->field($model, 'created_at')->textInput() ?>

    <?php 
        $questions = Question::find()->all();
        if(!$model->isNewRecord) foreach($questions as $i => $q) echo $form->field($d_mdl[$i], "score")->dropdownList(array_combine(range(1,$config['max_score']),range(1,$config['max_score'])))->label($q->question);
        else foreach($questions as $i => $q) echo $form->field($d_mdl, "[{$i}]score")->dropdownList(array_combine(range(1,$config['max_score']),range(1,$config['max_score'])))->label($q->question);
    ?>
    <?= Yii::$app->params['config']['tagging'] == 'y' ? $form->field($model, "tag")->checkboxList(ArrayHelper::map(Tag::find()->all(), 'id', 'tag'), ['separator'=>'&emsp;&emsp;', 'itemOptions' => ['class' =>'radio-inline', 'style'=>'margin:0 5px;']]) : '' ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
