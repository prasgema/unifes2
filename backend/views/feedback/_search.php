<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\FeedbackSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="feedback-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'round') ?>

    <?= $form->field($model, 'adjName') ?>

    <?= $form->field($model, 'giverAdjName') ?>

    <?= $form->field($model, 'giverTeamName') ?>

    <?php // echo $form->field($model, 'giver_status') ?>

    <?php // echo $form->field($model, 'agree_initial') ?>

    <?php // echo $form->field($model, 'agree_eventual') ?>

    <?php // echo $form->field($model, 'influence') ?>

    <?php // echo $form->field($model, 'comments') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
