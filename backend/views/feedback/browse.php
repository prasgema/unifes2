<?php

use kartik\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\FeedbackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Feedbacks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="feedback-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Feedback', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'round',
            'giverAdjName',
            'giverTeamName',
            'adjName',
            'averageScore',
            [
                'attribute'=>'status',
                'format'=>'raw',
                'value'=>function($model){
                    return $model->status=='n' ? Html::bsLabel('Normal') : 
                        ($model->status=='e' ? Html::bsLabel('Enquiries', Html::TYPE_INFO) : 
                            ($model->status=='a' ? Html::bsLabel('Backend', Html::TYPE_WARNING) : 
                                Html::bsLabel('Unidentified', Html::TYPE_DANGER)));
                }
            ],
            // 'giver_status',
            // 'agree_initial',
            // 'agree_eventual',
            // 'influence',
            // 'comments:ntext',
            // 'status',
            // 'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
