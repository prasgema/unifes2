<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\FeedbackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$attributes = ['id', 'name', 'institution', 'test'];
$columns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'attribute'=>'name',
        'format'=>'raw',
        'value'=>function($model){ return Html::a($model['name'],['watch', 'id'=>$model['id']]); }
    ],
    'institution',
    'test:raw'
];

foreach($rounds as $r){
    $attributes[] = 'R'.$r->round.':raw';
    $columns[] = 'R'.$r->round.':raw';
}

$attributes[] = 'score';
$columns[] = 'score';

$this->title = 'Adjudicator\'s Standing';
$this->params['breadcrumbs'][] = $this->title;
$provider = new ArrayDataProvider([
    'allModels' => $list,
    'sort' => [
        'attributes' => $attributes,
    ],
    'pagination' => false
]);
?>
<div class="feedback-index">

    <h1><?= Html::encode($this->title) ?>
    <?= Html::a('Mark On', ['index','marking'=>'on'], ['class'=>'btn btn-default']) ?>
    <?= Html::a('Mark Off', ['index','marking'=>'off'], ['class'=>'btn btn-default']) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $provider,
        'columns' => $columns,
    ]); ?>

</div>
