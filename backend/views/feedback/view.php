<?php

use kartik\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Feedback */

$this->title = 'Single Feedback '.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Feedbacks', 'url' => ['browse']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="feedback-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'round',
            'adj.name',
            'giver',
            'submissionStatus',
            'giverStatus',
            'agree:raw',
            'feedbackInfluence:raw:feedbackInfluence',
            'tagging:raw:tag',
            'averageScore',
            'scores:raw',
            'comments:ntext',
            'created_at',
        ],
    ]) ?>

</div>
