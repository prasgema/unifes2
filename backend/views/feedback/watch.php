<?php

use kartik\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Feedback */

$this->title = 'Feedback Records for '.$model->name;
$this->params['breadcrumbs'][] = ['label' => 'Feedbacks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="feedback-view">
    <h1><?= $model->name ?></h1>
    <h3><?= $model->institution ?>, test score : <?= $model->initial_score ?></h3>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'round',
            'giver',
            'giverStatus:raw:status',
            'averageScore',
            'scores:raw:score details',
            'agree:raw:agree',
            'feedbackInfluence:raw:influence',
            'tagging:raw:tag'
        ],
    ]); ?>

</div>
