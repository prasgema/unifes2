<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Language Status';
?>
<div class="tag-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'emptyText' => '',
	    'tableOptions' =>['class' => 'table table-striped'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'team_name',
            'debater_1',
            'languageStatus.d1s:raw',
            'debater_2',
            'languageStatus.d2s:raw',
            'debater_3',
            'languageStatus.d3s:raw',
            'languageStatus.appealed:raw:Appealed',

            [
            	'label'=>'',
            	'format'=>'raw',
            	'value'=>function($model){
            		return Html::a('View', ['languagestatus/view', 'id'=>$model->id], ['class'=>'btn btn-default']);
            	}
            ],
        ],
    ]); ?>

</div>