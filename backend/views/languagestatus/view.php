<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;

$appeals = new ActiveDataProvider([
    'query' => $model->languageStatus->getAppeals(),
    'pagination' => false,
]);

/* @var $this yii\web\View */
/* @var $model frontend\models\LanguageStatus */

$this->title = 'View Language Status for ID #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Language Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="language-status-view">

	<div class="row" style="border-bottom:1px grey solid;">
		<div class="col-lg-9">
		    <h2><?= $model->debater_1 ?> <?= $model->languageStatus->d1s ?></h2>

		    <?php
		    	$responses = explode(',', $model->languageStatus->speaker_1_interview);

		    	foreach($questions as $i => $q){
		    		list($x, $a) = explode(':', $q);
		    		$a = explode(',', $a);

		    		echo '<p style="font-weight:bold">'.$x.'</p>';
		    		echo '<p>'.$a[$responses[$i]].'</p>';
		    	}
		    ?>
		</div>

		<div class="col-lg-3">
		    <h1><div class="btn-group">
				<?= Html::a('Appr', ['languagestatus/approve', 'id'=>$model->id, 'status'=>'a', 'speaker'=>1], ['class'=>'btn btn-success']) ?>
				<?= Html::a('Itrvw', ['languagestatus/approve', 'id'=>$model->id, 'status'=>'i', 'speaker'=>1], ['class'=>'btn btn-warning']) ?>
				<?= Html::a('Rjct', ['languagestatus/approve', 'id'=>$model->id, 'status'=>'r', 'speaker'=>1], ['class'=>'btn btn-danger']) ?>
			</div></h1>
		</div>

    	<hr/>
	</div>

	<div class="row" style="border-bottom:1px grey solid;">
		<div class="col-lg-9">
		    <h2><?= $model->debater_2 ?> <?= $model->languageStatus->d2s ?></h2>

		    <?php
		    	$responses = explode(',', $model->languageStatus->speaker_2_interview);

		    	foreach($questions as $i => $q){
		    		list($x, $a) = explode(':', $q);
		    		$a = explode(',', $a);

		    		echo '<p style="font-weight:bold">'.$x.'</p>';
		    		echo '<p>'.$a[$responses[$i]].'</p>';
		    	}
		    ?>
		</div>

		<div class="col-lg-3">
		    <h1><div class="btn-group">
				<?= Html::a('Appr', ['languagestatus/approve', 'id'=>$model->id, 'status'=>'a', 'speaker'=>2], ['class'=>'btn btn-success']) ?>
				<?= Html::a('Itrvw', ['languagestatus/approve', 'id'=>$model->id, 'status'=>'i', 'speaker'=>2], ['class'=>'btn btn-warning']) ?>
				<?= Html::a('Rjct', ['languagestatus/approve', 'id'=>$model->id, 'status'=>'r', 'speaker'=>2], ['class'=>'btn btn-danger']) ?>
			</div></h1>
		</div>

    	<hr/>
	</div>

	<div class="row" style="border-bottom:1px grey solid;">
		<div class="col-lg-9">
		    <h2><?= $model->debater_3 ?> <?= $model->languageStatus->d3s ?></h2>

		    <?php
		    	$responses = explode(',', $model->languageStatus->speaker_3_interview);

		    	foreach($questions as $i => $q){
		    		list($x, $a) = explode(':', $q);
		    		$a = explode(',', $a);

		    		echo '<p style="font-weight:bold">'.$x.'</p>';
		    		echo '<p>'.$a[$responses[$i]].'</p>';
		    	}
		    ?>
		</div>

		<div class="col-lg-3">
		    <h1><div class="btn-group">
				<?= Html::a('Appr', ['languagestatus/approve', 'id'=>$model->id, 'status'=>'a', 'speaker'=>3], ['class'=>'btn btn-success']) ?>
				<?= Html::a('Itrvw', ['languagestatus/approve', 'id'=>$model->id, 'status'=>'i', 'speaker'=>3], ['class'=>'btn btn-warning']) ?>
				<?= Html::a('Rjct', ['languagestatus/approve', 'id'=>$model->id, 'status'=>'r', 'speaker'=>3], ['class'=>'btn btn-danger']) ?>
			</div></h1>
		</div>

    	<hr/>
	</div>

	<h2>Appeals</h2>
	<?= GridView::widget([
        'dataProvider' => $appeals,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'giverAdj.name:raw:ADJ',
            'giverTeam.team_name:raw:TEAM',
            'to_whom',
            'appeal',
        ],
    ]); ?>

</div>
