<?php

use kartik\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\ButtonGroup;
use yii\bootstrap\Progress;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rounds';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="round-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Round', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'round',
            'start_at',
            [
                'attribute'=>'status',
                'format'=>'raw',
                'value'=>function($model){
                    $s = $model->status;
                    return ButtonGroup::widget([
                        'buttons' => [
                            Html::a(Html::icon('hourglass').' Pending', ['round/pending', 'id'=>$model->round],['class'=> ($s=='p')?'btn btn-warning':'btn btn-default']),
                            Html::a(Html::icon('ok').' Open', ['round/open', 'id'=>$model->round],['class'=> ($s=='o')?'btn btn-primary':'btn btn-default']),
                            Html::a(Html::icon('volume-off').' Silent', ['round/silent', 'id'=>$model->round],['class'=> ($s=='s')?'btn btn-danger':'btn btn-default']),
                        ]
                    ]);
                },
                'options' => [
                    'column'
                ]
            ],
            [
                'attribute'=>'progressTeam',
                'format'=>'raw',
                'value'=>function($model){
                    return Progress::widget([
                        'percent' => $model->progressTeam,
                        'barOptions' => ['class' => 'progress-bar-success'],
                        'options' => ['class' => 'active progress-striped'],
                        'label' => round($model->progressTeam,2).'%',
                    ]);
                }
            ],
            [
                'attribute'=>'progressAdj',
                'format'=>'raw',
                'value'=>function($model){
                    return Progress::widget([
                        'percent' => $model->progressAdj,
                        'barOptions' => ['class' => 'progress-bar-primary'],
                        'options' => ['class' => 'active progress-striped'],
                        'label' => round($model->progressAdj,2).'%',
                    ]);
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
