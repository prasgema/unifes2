<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Round */

$this->title = 'Update Round: ' . ' ' . $model->round;
$this->params['breadcrumbs'][] = ['label' => 'Rounds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->round, 'url' => ['view', 'id' => $model->round]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="round-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
