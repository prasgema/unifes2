<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model frontend\models\Round */

$this->title = 'Update Allocation ' . ' ' . $model->debate->id;
$this->params['breadcrumbs'][] = ['label' => 'Rounds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Rounds'.$model->debate->round, 'url' => ['view', 'id' => $model->debate->round]];
$this->params['breadcrumbs'][] = 'Update Allocation';
?>
<div class="round-update">

    <h1><?= Html::encode($this->title) ?></h1>

	<div class="round-form">

	    <?php $form = ActiveForm::begin(); ?>

		<?= $form->field($model, 'adj_id')->widget(Select2::classname(),[
			'data' => ArrayHelper::map($adj, 'id', 'nameInst')
		]) ?>

	    <?= $form->field($model, 'debate_id')->widget(Select2::classname(),[
			'data' => ArrayHelper::map($debate, 'id', 'allocation')
		]) ?>

	    <?= $form->field($model, 'status')->dropdownList(['c' => 'Chair','p' => 'Panelist','t' => 'Trainee']) ?>

	    <div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>

	    <?php ActiveForm::end(); ?>

	</div>

</div>
