<?php

use kartik\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\bootstrap\ButtonGroup;

/* @var $this yii\web\View */
/* @var $model frontend\models\Round */

$this->title = $model->round;
$this->params['breadcrumbs'][] = ['label' => 'Rounds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="round-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= ButtonGroup::widget([
            'buttons' => [
                Html::a('Edit Motion', ['update', 'id' => $model->round], ['class' => 'btn btn-primary']),
                Html::a('Import Debate Data', ['import', 'round' => $model->round], ['class' => 'btn btn-success']),
            ]
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'round',
            'status',
            'start_at',
            'motion:ntext',
        ],
    ]) ?>

    <?= GridView::widget([
        'dataProvider' => $adj,
        'tableOptions'=>['class'=>'table table-striped', 'style'=>'font-size:small;'],
        'columns' => [
            'debate.round',
            'debate.room',
            'debate.teamOG.team_name',
            'debate.teamOO.team_name',
            'debate.teamCG.team_name',
            'debate.teamCO.team_name',
            'adjudicator.nameInst',
            [
                'attribute'=>'status',
                'format'=>'raw',
                'value'=>function($model){
                    return $model->status=='c' ? Html::bsLabel('Chair', Html::TYPE_SUCCESS) : 
                        ($model->status=='p' ? Html::bsLabel('Panel', Html::TYPE_INFO) : 
                            ($model->status=='t' ? Html::bsLabel('Trainee', Html::TYPE_WARNING) : 
                                Html::bsLabel('Unidentified', Html::TYPE_DANGER)));
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        return Html::a(Html::icon('pencil'), 
                            ['updatealloc', 'adj_id'=>$model->adj_id,'debate_id'=>$model->debate_id]
                        );
                    }
                ],
            ],
        ],
    ]);
    ?>
</div>
