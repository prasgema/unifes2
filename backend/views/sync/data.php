<?php
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */

$cols = array_keys($online[0]);
$online = new ArrayDataProvider([
    'allModels' => $online,
    'pagination' => false
]);
$local = new ArrayDataProvider([
    'allModels' => $local,
    'pagination' => false
]);
?>
<h1>sync/adjudicator</h1>

<div class="col-lg-6"><?= Html::a(' >> Sync Local to Online >> ', ['execute', 'what'=>'adjudicator', 'ft'=>'l2o'], ['class'=>'btn btn-success', 'style'=>'width:100%']) ?></div>
<div class="col-lg-6"><?= Html::a(' << Sync Online to Local <<', ['execute', 'what'=>'adjudicator', 'ft'=>'o2l'], ['class'=>'btn btn-warning', 'style'=>'width:100%']) ?></div>

<div class="col-lg-6 table-responsive">
    <h2>Local Data</h2>
    <?= GridView::widget([
        'dataProvider' => $local,
        'emptyCell'=>'',
        'columns' => $cols,
        'summary'=>false,
    ]) ?>
</div>
<div class="col-lg-6 table-responsive">
    <h2>Online Data</h2>
    <?= GridView::widget([
        'dataProvider' => $online,
        'emptyCell'=>'',
        'columns' => $cols,
        'summary'=>false,
    ]) ?>
</div>
