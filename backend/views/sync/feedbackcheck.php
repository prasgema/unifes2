<?php
/* @var $this yii\web\View */
use frontend\models\Adjudicator;
use frontend\models\Team;

$cols = array_keys($online[0]);
$odata = []; $ldata = [];

foreach($online as $o){
	$team = Team::findOne($o['giver_team_id']);
	$adj = Adjudicator::findOne($o['giver_adj_id']);
	$s = Adjudicator::findOne($o['adj_id'])->name;

	if($team == null) $team = $o['giver_team_id'];
	else $team = $team->team_name;

	if($adj == null) $adj = $o['giver_adj_id'];
	else $adj = $adj->name;

	$odata[] = 'R'.$o['round'].' > '.$team.$adj.' > '.$s.' = '.$o['detail'].' > '.$o['tag'];
}
foreach($local as $l){
	$team = Team::findOne($l['giver_team_id']);
	$adj = Adjudicator::findOne($l['giver_adj_id']);
	$s = Adjudicator::findOne($l['adj_id'])->name;

	if($team == null) $team = $l['giver_team_id'];
	else $team = $team->team_name;

	if($adj == null) $adj = $l['giver_adj_id'];
	else $adj = $adj->name;

	$ldata[] = 'R'.$l['round'].' > '.$team.$adj.' > '.$s.' = '.$l['detail'].' > '.$l['tag'];
}

?>
<h1>sync/feedback</h1>

<div class="col-lg-6 table-responsive">
    <h2>Local Data</h2>
    <?php
    	foreach( array_diff($ldata, $odata) as $l) echo $l.'<br/>';
    ?>
</div>
<div class="col-lg-6 table-responsive">
    <h2>Online Data</h2>
    <?php
    	foreach( array_diff($odata, $ldata) as $o) echo $o.'<br/>';
    ?>
</div>
