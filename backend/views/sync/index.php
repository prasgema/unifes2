<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
?>
<h1>sync/index</h1>

<p>
	FIRST TIER
	<?= Html::a('Adjudicator', ['sync/adjudicator'], ['class'=>'btn btn-default']) ?>
	<?= Html::a('Team', ['sync/team'], ['class'=>'btn btn-default']) ?>
	<?= Html::a('Round', ['sync/round'], ['class'=>'btn btn-default']) ?>
</p>
<hr/>
<p>
	SECOND TIER
	<?= Html::a('Debate', ['sync/debate'], ['class'=>'btn btn-default']) ?>
	<?= Html::a('Debate Adjudicators', ['sync/debateadjudicator'], ['class'=>'btn btn-default']) ?>
</p>
<hr/>
<p>
	THIRD TIER
	<?= Html::a('Feedbacks', ['sync/feedback'], ['class'=>'btn btn-default']) ?>
</p>
<hr/>
<p>
	CHECK LAYER
	<?= Html::a('Feedback Conflict Check', ['sync/feedbackcheck'], ['class'=>'btn btn-default']) ?>
</p>
<hr/>
<p>
	TABBYCAT
	<?= Html::a('Commit Feedback to Tabbycat', ['sync/commitfeedback'], ['class'=>'btn btn-default']) ?>
</p>
<p>
	TABBIE
	<?= Html::a('Commit Scores to Tabbie', ['sync/commitfeedback'], ['class'=>'btn btn-default', 'disabled'=>true]) ?>
	(coming soon)
</p>