<?php
use yii\data\ArrayDataProvider;
use yii\grid\GridView;

/* @var $this yii\web\View */
$online = new ArrayDataProvider([
    'allModels' => $online,
    'pagination' => false
]);

$local = new ArrayDataProvider([
    'allModels' => $local,
    'pagination' => false
]);
?>
<h1>sync/round</h1>

<div class="col-lg-6">
    <?= GridView::widget([
        'dataProvider' => $online,
        'emptyCell'=>'',
        'columns' => [
        	'round',
        	'status',
        	'start_at',
        	'motion',
        ],
        'summary'=>false,
    ]) ?>
</div>

<div class="col-lg-6">
    <?= GridView::widget([
        'dataProvider' => $local,
        'emptyCell'=>'',
        'columns' => [
        	'round',
        	'status',
        	'start_at',
        	'motion',
        ],
        'summary'=>false,
    ]) ?>
</div>