<?php
use yii\data\ArrayDataProvider;
use yii\grid\GridView;

/* @var $this yii\web\View */

$localcheck = []; $onlinecheck = [];

foreach($local as $i => $l){
	if( !in_array($l, $online) ) $localcheck[$i] = 'CHECK!';
	else  $localcheck[$i] = '';
}
foreach($online as $i => $o){
	if( !in_array($o, $local) ) $onlinecheck[$i] = 'CHECK!';
	else $onlinecheck[$i] = '';
}
foreach($local as $i => &$l) $l['CH'] = $localcheck[$i];
foreach($online as $i => &$o) $o['CH'] = $onlinecheck[$i];

$online = new ArrayDataProvider([
    'allModels' => $online,
    'pagination' => false
]);

$local = new ArrayDataProvider([
    'allModels' => $local,
    'pagination' => false
]);
?>
<h1>sync/team</h1>

<div class="col-lg-6">
	<h2>ONLINE</h2>
    <?= GridView::widget([
        'dataProvider' => $online,
        'emptyCell'=>'',
        'columns' => [
        	'id',
        	'team_name',
        	'debater_1',
        	'debater_2',
        	'debater_3',
        	//'unique_code',
        	'CH',
        ],
        'summary'=>false,
    ]) ?>
</div>

<div class="col-lg-6">
	<h2>LOCAL</h2>
    <?= GridView::widget([
        'dataProvider' => $local,
        'emptyCell'=>'',
        'columns' => [
        	'id',
        	'team_name',
        	'debater_1',
        	'debater_2',
        	'debater_3',
        	//'unique_code',
        	'CH',
        ],
        'summary'=>false,
    ]) ?>
</div>