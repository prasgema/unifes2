<?php

use kartik\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\ButtonGroup;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TeamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Teams';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= ButtonGroup::widget([
            'buttons' => [
                Html::a('Create Team', ['create'], ['class' => 'btn btn-primary']),
                Html::a('Import Debate Data', ['import'], ['class' => 'btn btn-success']),
            ]
        ]) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'team_name',
            'debater_1',
            'debater_2',
            'debater_3',
            // 'unique_code',
            [
                'label'=>'Status',
                'format'=>'raw',
                'value'=> function($model){
                    if($model->status == 'o') return Html::a(Html::bsLabel('OPEN', Html::TYPE_SUCCESS), ['team/status', 'id'=>$model->id]);
                    else if($model->status == 'l') return Html::a(Html::bsLabel('LOCK', Html::TYPE_DANGER), ['team/status', 'id'=>$model->id]);
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
