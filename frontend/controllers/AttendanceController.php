<?php

namespace frontend\controllers;
use frontend\models\Team;
use frontend\models\Adjudicator;
use frontend\models\AttendanceTeam;
use frontend\models\AttendanceAdj;
use frontend\models\Round;
use yii\helpers\Html;
use yii\helpers\Url;

class AttendanceController extends \yii\web\Controller
{
    private function getLatestRound(){
        return Round::find()->where('status != \'p\'')->orderBy('round DESC')->one()->round;
    }

    private function getCheckedAdj(){
        $round = $this->getLatestRound();
        $attAdj = AttendanceAdj::find()->where(['round'=>$round])->all();
        $id_list = [];
        foreach($attAdj as $a) $id_list[] = $a->adjudicator_id;
        return $id_list;
    }

    private function getCheckedTeam(){
        $round = $this->getLatestRound();
        $attTeam = AttendanceTeam::find()->where(['round'=>$round])->all();
        $id_list = [];
        foreach($attTeam as $a) $id_list[] = $a->team_id;
        return $id_list;
    }

    public function actionAdjudicator($id, $round=null)
    {
        if($round == null) $round = $this->getLatestRound();
        $att = AttendanceAdj::find()->where(['adjudicator_id'=>$id, 'round'=>$round])->one();

        if($att != null) return $this->render('failed');
        else {
            $att = new AttendanceAdj();

            $att->adjudicator_id = $id;
            $att->round = $round;
            $att->check_in = Date('Y-m-d H:i:s');

            if($att->save()) return $this->render('attendance');
            else return $this->render('failed');
        }
    }

    public function actionTeam($id, $round=null)
    {
        if($round == null) $round = $this->getLatestRound();
        $att = AttendanceTeam::find()->where(['team_id'=>$id, 'round'=>$round])->one();

        if($att != null) return $this->render('failed');
        else {
            $att = new AttendanceTeam();

            $att->team_id = $id;
            $att->round = $round;
            $att->check_in = Date('Y-m-d H:i:s');

            if($att->save()) return $this->render('attendance');
            else return $this->render('failed');
        }
    }

    public function actionIndex(){
        Url::remember();
        if(isset($_POST['code']) && $_POST['code']!=null){
            print_r($_POST['code']);

            $team = Team::find()->where(['unique_code'=>$_POST['code']])->one();
            if($team == null){
                $adj = Adjudicator::find()->where(['unique_code'=>$_POST['code']])->one();
                if($adj == null) return $this->render('failed');
                $this->redirect(['adjudicator','id'=>$adj->id]);
            }
            else $this->redirect(['team','id'=>$team->id]);

        } else {
            return $this->render('index', ['round' => $this->getLatestRound(),]);
        }
    }

    public function actionListing()
    {
        Url::remember();
        if(isset($_POST['giver']) && $_POST['giver']!=null){
            print_r($_POST['giver']);

            $team = Team::find()->where(['team_name'=>$_POST['giver']])->one();
            if($team == null){
                $adj = Adjudicator::find()->where("CONCAT(name,' (',institution,')') = '{$_POST['giver']}'")->one();
                if($adj == null) return $this->render('failed');
                $this->redirect(['adjudicator','id'=>$adj->id]);
            }
            else $this->redirect(['team','id'=>$team->id]);

        } else {
            $teams = Team::find()->orderBy('team_name')->all();
            $adjs = Adjudicator::find()->orderBy('name')->all();

            $list = array();

            foreach($teams as $team) {
                $firstLetter = substr($team->team_name, 0, 1);
                if(!isset($list[$firstLetter])) $list[$firstLetter] = [ 'idx'=>$firstLetter, 'team'=>Html::a($team->team_name,['team','id'=>$team->id],['class'=>in_array($team->id, $this->getCheckedTeam()) ? 'btn btn-success' : 'btn btn-default','style'=>'margin:1px']), 'adjudicator'=>''];
                else $list[$firstLetter]['team'].=Html::a($team->team_name,['team','id'=>$team->id],['class'=> in_array($team->id, $this->getCheckedTeam()) ? 'btn btn-success' : 'btn btn-default','style'=>'margin:1px']);
            }
            foreach($adjs as $adj){
                $firstLetter = substr($adj->name, 0, 1);
                if(!isset($list[$firstLetter])) $list[$firstLetter] = [ 'idx'=>$firstLetter, 'team'=>'', 'adjudicator'=>Html::a($adj->name.' ('.$adj->institution.')',['adjudicator','id'=>$adj->id],['class'=>in_array($adj->id, $this->getCheckedAdj()) ? 'btn btn-success' : 'btn btn-default','style'=>'margin:1px'])];
                else $list[$firstLetter]['adjudicator'].=Html::a($adj->name.' ('.$adj->institution.')',['adjudicator','id'=>$adj->id],['class'=>in_array($adj->id, $this->getCheckedAdj()) ? 'btn btn-success' : 'btn btn-default','style'=>'margin:1px']);
            }

            ksort($list);

            return $this->render('listing', [
                'teams' => $teams,
                'adjs' => $adjs,
                'list' => $list,
                'round' => $this->getLatestRound(),
            ]);
        }
    }

}
