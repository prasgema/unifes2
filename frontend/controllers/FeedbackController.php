<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Feedback;
use frontend\models\FeedbackDetail;
use frontend\models\FeedbackTag;
use frontend\models\Enquiries;
use frontend\models\EnquiriesDetail;
use frontend\models\EnquiriesTag;
use frontend\models\Round;
use frontend\models\Debate;
use frontend\models\DebateAdj;
use frontend\models\Team;
use frontend\models\Adjudicator;
use frontend\models\Question;
use frontend\models\Tag;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Session;
use yii\db\Query;
use yii\filters\VerbFilter;
use kartik\helpers\Html;

/**
 * FeedbackController implements the CRUD actions for Feedback model.
 */
class FeedbackController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    private function getLatestFeedbackRound($method = null, $giver = null){
        $adjfeedback_on_silent = Yii::$app->params['config']['adjfeedback_on_silent'];
        if($method == null || $giver == null) $round = Round::find()->where('status != \'s\' AND status != \'p\'')->orderBy('round DESC')->one()->round; 
        else{
            if($method == 'd2a') $round = Round::find()->where("status != 's' AND status != 'p' AND round NOT IN (SELECT DISTINCT f.round FROM feedback f WHERE f.giver_team_id = {$giver})")->orderBy('round DESC')->one()->round; 
            
            if($method == 'a2a' && $adjfeedback_on_silent == 'y') $round = Round::find()->where("status != 'p' AND round NOT IN (SELECT DISTINCT f.round FROM feedback f WHERE f.giver_adj_id = {$giver})")->orderBy('round DESC')->one()->round; 
            else if($method == 'a2a') $round = Round::find()->where("status != 's' AND status != 'p' AND round NOT IN (SELECT DISTINCT f.round FROM feedback f WHERE f.giver_adj_id = {$giver})")->orderBy('round DESC')->one()->round; 
        }

        if($round == null) $round = Round::find()->where('status != \'s\' AND status != \'p\'')->orderBy('round DESC')->one()->round; 

        return $round;
    }

    /**
     * Lists all Feedback models.
     * @return mixed
     */
    public function actionIndex()
    {   
        if(isset($_POST['giver']) && $_POST['giver']!=null){
            print_r($_POST['giver']);

            $team = Team::find()->where(['team_name'=>$_POST['giver']])->one();
            if($team == null){
                $adj = Adjudicator::find()->where("CONCAT(name,' (',institution,')') = '{$_POST['giver']}'")->one();
                $this->redirect(['create','method'=>'a2a','giver'=>$adj->id]);
            }
            else $this->redirect(['create','method'=>'d2a','giver'=>$team->id]);

        } else {
            // $session = new Session();
            // $session->open();
            // $session->destroy();

            $teams = Team::find()->orderBy('team_name')->all();
            $adjs = Adjudicator::find()->orderBy('name')->all();

            $list = array();

            foreach($teams as $team) {
                $firstLetter = substr($team->team_name, 0, 1);
                if(!isset($list[$firstLetter])) $list[$firstLetter] = [ 'idx'=>$firstLetter, 'team'=>Html::a($team->display,['create','method'=>'d2a','giver'=>$team->id],['class'=>'btn btn-default','style'=>'margin:1px']), 'adjudicator'=>''];
                else $list[$firstLetter]['team'].=Html::a($team->display,['create','method'=>'d2a','giver'=>$team->id],['class'=>'btn btn-default','style'=>'margin:1px']);
            }
            foreach($adjs as $adj){
                $firstLetter = substr($adj->name, 0, 1);
                if(!isset($list[$firstLetter])) $list[$firstLetter] = [ 'idx'=>$firstLetter, 'team'=>'', 'adjudicator'=>Html::a($adj->display,['create','method'=>'a2a','giver'=>$adj->id],['class'=>'btn btn-default','style'=>'margin:1px'])];
                else $list[$firstLetter]['adjudicator'].=Html::a($adj->display,['create','method'=>'a2a','giver'=>$adj->id],['class'=>'btn btn-default','style'=>'margin:1px']);
            }

            ksort($list);

            return $this->render('index', [
                'teams' => $teams,
                'adjs' => $adjs,
                'list' => $list,
            ]);
        }
    }

    public function actionThankyou(){
        $session = new Session();
        $session->open();
        $session->destroy();

        return $this->render('thankyou');
    }

    /**
     * Displays a single Feedback model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionVerify($method = null, $giver = null, $round = null)
    {
        //tolak kalo ga tau mau ngapain
        if($giver == null || $method == null || $round == null) return $this->redirect(['index']);

        //cari siapa yang mau di verifikasi
        if($method == 'd2a') $model = Team::findOne($giver);
        else if($method == 'a2a') $model = Adjudicator::findOne($giver);

        if(isset($_POST['unique-code'])){
            //kalo verifikasi berhasil, bikin session. kalo salah, balik dan kasi pesan error
            if($_POST['unique-code'] === $model->unique_code){
                $session = new Session();
                $session->open();
                $session['user'] = $model;
                $session['method'] = $method;

                return $this->redirect(['create', 'method'=>$method, 'giver'=>$giver, 'round'=>$round]);
            }
            else Yii::$app->session->setFlash('error', 'Wrong Unique Code. Try Again!');
        }

        return $this->render('verify', [
            'model' => $model,
            'method' => $method,
            'giver' => $giver,
            'round' => $round,
        ]); 
    }
    /**
     * Creates a new Feedback model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($method = null, $giver = null, $round = null)
    {
        //load config
        $config = Yii::$app->params['config'];

        //cek apa request bener atau gak. kalo giver method ga ada: tolak, kalo round ga ada, set ke round terakhir
        $latestRound = $this->getLatestFeedbackRound($method, $giver);
        if($giver == null || $method == null || ($round == null && $latestRound ==null)) return $this->redirect(['index']);
        if ($round == null) return $this->redirect(['create', 'method'=>$method, 'giver'=>$giver, 'round'=>$latestRound]);

        //JIKA PERLU UNIQUE CODE: check session. orang ini uda isi unique code kah?
        if($config['unique_code']=='y'){
            $session = new Session();
            $session->open();
            if(!isset($session['user']) || $session['user']->id != $giver || $session['method'] != $method) $this->redirect(['verify', 'method'=>$method, 'giver'=>$giver, 'round'=>$round]);
        }

        //get silent rounds

        //cari debat yang relevan sesuai status orangnya. kalo ga ke register di debat ini, throw error screen
        if($method == 'd2a'){ $debate = Debate::find()->where('(team_id_og = '.$giver.' OR team_id_oo = '.$giver.' OR team_id_cg = '.$giver.' OR team_id_co = '.$giver.') AND round = '.$round)->one(); }
        else if($method == 'a2a'){ $d_adj = DebateAdj::find()->where('adj_id = '.$giver.' AND debate_id IN (SELECT id FROM debate WHERE round = '.$round.')')->one(); $d_adj == null ? $debate = null : $debate = $d_adj->debate; }
        
        //retreive yang pernah diisi sama orang ini && check feedback uda diisi atau belum: render error kalo udah.
        //config add: cek kalo orang ini ga perlu masukin feedback di round tertentu
        if($method == 'd2a'){
            //kalo debater perlu isi feedback, cari round yang gak silent dan doi ikut
            //kalo ga perlu isi feedback, otomatis semua round dianggap sudah
            if($config['team_to'] != '?') $feedback_done = \Yii::$app->db->createCommand('SELECT DISTINCT round FROM debate WHERE (team_id_og = '.$giver.' OR team_id_oo = '.$giver.' OR team_id_cg = '.$giver.' OR team_id_co = '.$giver.') AND round NOT IN (SELECT round FROM round WHERE status != \'s\') OR round IN (SELECT round FROM feedback WHERE giver_team_id = '.$giver.') ORDER BY round')->queryAll();
            else $feedback_done = \Yii::$app->db->createCommand('SELECT round FROM round')->queryAll();
        }
        else if($method == 'a2a'){
            //kalo adjes ga perlu isi feedback, semua round dianggap udah
            //say misalnya mereka panel ketika panel ga perlu isi feedback: atau mereka single chair
            //ini ditolak jika: kondisi 1: feedback udah diisi
            //                          2: dia adalah single adj
            //                          3: status waktu itu adalah status yang ga bisa input skor
            $query = 'SELECT r.round FROM round r WHERE r.round IN (SELECT f.round FROM feedback f WHERE f.giver_adj_id = '.$giver.')
                OR r.round IN (SELECT dc.round FROM debate dc JOIN debate_adj dac ON dac.debate_id=dc.id WHERE dac.adj_id = '.$giver.' AND (SELECT COUNT(*) FROM debate_adj dax WHERE dax.debate_id = dc.id) = 1)
                OR r.round IN (SELECT d.round FROM debate_adj da JOIN debate d ON da.debate_id = d.id WHERE da.adj_id = '.$giver.' AND da.status IN (';

            if($config['chair_to'] == '?') $query.='\'c\', ' ;
            if($config['panel_to'] == '?') $query.='\'p\', ' ;
            if($config['trainee_to'] == '?') $query.='\'t\', ' ;
            $query.='\'*\', \'?\'))';

             // kalo silent round dan ga ada adju feedback on silent, jangan dikasi isi
            if($config['adjfeedback_on_silent'] == 'n')  $query.=' OR status = \'s\'';

            $feedback_done = \Yii::$app->db->createCommand($query)->queryAll();
        }

        //error
        if($debate == null) return $this->render('nodebate', ['latestRound'=>$latestRound,'feedback_done' => $feedback_done, 'method' => $method, 'giver' => $giver, 'round' => $round,]);
        if(in_array(['round'=>$round],$feedback_done)) return $this->render('feedbackfilled', ['latestRound'=>$latestRound, 'feedback_done' => $feedback_done, 'method' => $method, 'giver' => $giver, 'round' => $round,]);

        
        //kumpulin properties yang penting ditampilin di form
        $question = Question::find()->all();
        if($method == 'd2a') $tag = Tag::find()->where('tag_user = \'d\' OR tag_user = \'*\'')->all();
        else if($method == 'a2a') $tag = Tag::find()->where('tag_user = \''.$d_adj->status.'\' OR tag_user = \'a\' OR tag_user = \'*\'')->all();
        $model = new Feedback();
        $d_mdl = new FeedbackDetail();

        if (Yii::$app->request->post()) { //request jalan
            foreach($debate->debateAdj as $i => $a){
                if(isset($_POST['Feedback'][$i])){
                    //isi feedback secara umum
                    $model = new Feedback();
                    $model->attributes = $_POST['Feedback'][$i];
                    
                    //kalo dari debater, agree initial dijadiin default + sisanya dikasi label sesuai posisi mereka di debat
                    //kalo dari adju, status adalah posisi mereka di debat
                    if($method == 'd2a'){
                        $model->agree_initial = '*';
                        $model->giver_team_id = $giver;
                        if($giver == $debate->team_id_og) $model->giver_status = 'og';
                        else if($giver == $debate->team_id_oo) $model->giver_status = 'oo';
                        else if($giver == $debate->team_id_cg) $model->giver_status = 'cg';
                        else if($giver == $debate->team_id_co) $model->giver_status = 'co';
                    }
                    else if($method == 'a2a'){
                        $model->giver_adj_id = $giver;
                        $model->giver_status = $d_adj->status;
                    }

                    //isi common properties feedback yang seharusnya ada
                    $model->influence = implode($model->influences);
                    $model->status = 'n'; //normal submission
                    $model->round = $round;
                    $model->created_at = date('Y-m-d H:i:s');

                    //kalo feedback di save, save juga tagging dan detailed scores
                    if($model->save(false)){
                        foreach($question as $j => $q){
                            $d_mdl = new FeedbackDetail();
                            $d_mdl->feedback_id = $model->id;
                            $d_mdl->question_id = $q->id;
                            $d_mdl->score = $_POST['FeedbackDetail'][$i][$j]['score'];

                            $d_mdl->save(false);
                        } //foreach question

                        if($config['tagging'] == 'y'){
                            foreach($model->tag as $j => $t){
                                $t_mdl = new FeedbackTag();
                                $t_mdl->feedback_id = $model->id;
                                $t_mdl->tag_id = $t;

                                $t_mdl->save(false);
                            } //foreach tag
                        } //if ada tagging
                    } //if model save
                } //kalo di inputan ada isinyatau gak
                else if(!$_POST['Feedback'][$i] && $method == 'a2a' && $a->adj_id != $giver){
                    //validasi, kalo seandainya ini beneran orang ga masuk; bukan si yang ngasi feedback
                    //record disimpen without any value
                    $model = new Feedback();
                    $model->round = $round;
                    $model->adj_id = $a->adj_id;
                    $model->giver_adj_id = $giver;
                    $model->giver_status = $d_adj->status;
                    $model->agree_initial = ' ';
                    $model->agree_eventual = ' ';
                    $model->influence = ' ';
                    $model->comments = ' ';
                    $model->status = 'x'; //not attending the round
                    $model->created_at = date('Y-m-d H:i:s');

                    if($model->save(false)){
                        foreach($question as $j => $q){
                            $d_mdl = new FeedbackDetail();
                            $d_mdl->feedback_id = $model->id;
                            $d_mdl->question_id = $q->id;
                            $d_mdl->score = 0;

                            $d_mdl->save(false);
                        } //foreach question
                    }
                }

            } //foreach semua input adju
            //kalo ini submission terakhir dia, destroy session!!
            if(isset($_POST['end'])){
                if($config['unique_code']=='y') $session->destroy();
                return $this->redirect(['thankyou']);
            } else {
                return $this->redirect(['create', 'method'=>$method, 'giver'=>$giver, 'round'=>$this->getLatestFeedbackRound($method, $giver)]);
            } //apa ini end a
        } else {
            return $this->render('create', [
                'config' => $config,
                'model' => $model,
                'd_mdl' => $d_mdl,
                'method' => $method,
                'giver' => $giver,
                'round' => $round,

                'question' => $question,
                'tag' => $tag,
                'feedback_done' => $feedback_done,
                'latestRound' => $latestRound,
                'debate' => $debate,
            ]);
        } //post request
    }

    public function actionEnquiries($method = null, $giver = null){
        //load config
        $config = Yii::$app->params['config'];

        //lempar kalo ga tau ini siapa
        if($giver == null || $method == null) return $this->redirect(['index']);
        
        $question = Question::find()->all();

        if (isset($_POST['adjudicator'])) {
            //kumpulin properties yang penting ditampilin di form
            if($method == 'd2a') $tag = Tag::find()->where('tag_user = \'d\' OR tag_user = \'*\'')->all();
            else if($method == 'a2a') $tag = Tag::find()->where('tag_user = \'c\' OR tag_user = \'p\' OR tag_user = \'a\' OR tag_user = \'*\'')->all();
            $model = new Enquiries();
            $d_mdl = new EnquiriesDetail();

            foreach($_POST['adjudicator'] as $i=>$a){ $adj[$i] = new DebateAdj(); $adj[$i]->adj_id = $a['adj_id'];}

            return $this->render('enquiries', [
                'config' => Yii::$app->params['config'],
                'model' => $model,
                'd_mdl' => $d_mdl,
                'method' => $method,
                'giver' => $giver,

                'question' => $question,
                'tag' => $tag,
                'adj' => $adj,
            ]);
        } else if (isset($_POST['Enquiries'])) {
            foreach($_POST['Enquiries'] as $i=>$a){
                if(is_numeric($i)){
                    //isi feedback secara umum
                    $model = new Enquiries();
                    $model->attributes = $_POST['Enquiries'][$i];
                    
                    //kalo dari debater, agree initial dijadiin default + sisanya dikasi label sesuai posisi mereka di debat
                    //kalo dari adju, status adalah posisi mereka di debat
                    if($method == 'd2a'){
                        $model->agree_initial = '*';
                        $model->giver_team_id = $giver;
                        $debate = Debate::find()->where('(team_id_og = '.$giver.' OR team_id_oo = '.$giver.' OR team_id_cg = '.$giver.' OR team_id_co = '.$giver.') AND round = '.$_POST['Enquiries']['round'])->one();
                        if($debate == null) $model->giver_status = '?';
                        else if($giver == $debate->team_id_og) $model->giver_status = 'og';
                        else if($giver == $debate->team_id_oo) $model->giver_status = 'oo';
                        else if($giver == $debate->team_id_cg) $model->giver_status = 'cg';
                        else if($giver == $debate->team_id_co) $model->giver_status = 'co';
                    }
                    else if($method == 'a2a'){
                        $d_adj = DebateAdj::find()->where('adj_id = '.$giver.' AND debate_id IN (SELECT debate_id FROM debate WHERE round = '.$round.')')->one(); 
                        $model->giver_adj_id = $giver;
                        if($d_adj == null) $model->giver_status = $d_adj->status;
                        else $model->giver_status = '?';
                    }

                    //isi common properties feedback yang seharusnya ada
                    $model->round = $_POST['Enquiries']['round'];
                    $model->room = $_POST['Enquiries']['room'];
                    $model->influence = implode($model->influences);
                    $model->adj_id = $a['adj_id'];
                    $model->status = 'o'; //default: open
                    $model->created_at = date('Y-m-d H:i:s');

                    //kalo feedback di save, save juga tagging dan detailed scores
                    if($model->save(false)){
                        foreach($question as $j => $q){
                            $d_mdl = new EnquiriesDetail();
                            $d_mdl->enquiries_id = $model->id;
                            $d_mdl->question_id = $q->id;
                            $d_mdl->score = $_POST['EnquiriesDetail'][$i][$j]['score'];

                            $d_mdl->save(false);
                        } //foreach question

                        if(config['tagging']=='y'){
                            foreach($model->tag as $j => $t){
                                $t_mdl = new EnquiriesTag();
                                $t_mdl->enquiries_id = $model->id;
                                $t_mdl->tag_id = $t;

                                $t_mdl->save(false);
                            } //foreach tag
                        }
                    } //if model save
                } //kalo di inputan ada isinya
            } //foreach semua adj

            if($config['unique_code']=='y'){
                $session = new Session();
                $session->open();
                $session->destroy();
            }

            return $this->redirect(['thankyou']);
        } else {
            $panels = isset($_GET['panels'])? $_GET['panels'] : 1;

            return $this->render('chooseadj', [
                'method' => $method,
                'giver' => $giver,
                'adjudicator'=>Adjudicator::find()->all(),
                'panels' => $panels
            ]);
        }
    }

    /**
     * Updates an existing Feedback model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Feedback model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDisplay($round = null){ 
        if($round == null) $round = $this->getLatestFeedbackRound();
        $round = Round::findOne($round);
        $query = new Query();

        $mTeams = $query->select('team_name')->from('team t')
            ->where("(SELECT COUNT(*) FROM feedback f WHERE f.giver_team_id = t.id AND round = {$round->round})=0")->all();
        $mAdjs = \Yii::$app->db->createCommand("
                SELECT CONCAT( `name`,' (',`institution`,')' ) name
                FROM adjudicator a
                WHERE (SELECT COUNT(*) FROM feedback f WHERE a.id = f.giver_adj_id AND round = {$round->round})=0
            ")->queryAll();

        $ma = ''; $mt='';
        foreach($mTeams as $team) $mt.=($team['team_name'].', ');
        foreach($mAdjs as $adj) $ma.=($adj['name'].', ');

        return $this->render('display', [
            'round'=>$round,
            'mTeams'=>$mt,
            'mAdjs'=>$ma
        ]);
    }

    public function actionWallofshame(){
        $adj = Adjudicator::find()->all();
        $team = Team::find()->all();

        return $this->render('wallofshame', [
            'adj'=>$adj,
            'team'=>$team
        ]);
    }

    /**
     * Finds the Feedback model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Feedback the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Feedback::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

