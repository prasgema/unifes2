<?php
namespace frontend\controllers;

use frontend\models\Team;
use frontend\models\Adjudicator;
use frontend\models\LanguageStatusAppeal;
use frontend\models\LanguageStatus;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use Yii;

class LanguagestatusController extends \yii\web\Controller
{
    public function actionAppeal($id)
    {
        $model = $this->findTeam($id);
        $appeal = new LanguageStatusAppeal();

        if(isset($_POST['giver']) && isset($_POST['unique_code'])){
            $team = Team::find()->where(['team_name'=>$_POST['giver']])->one();

            if($team == null){
                $adj = Adjudicator::find()->where("CONCAT(name,' (',institution,')') = '{$_POST['giver']}'")->one();
                $appeal->giver_adj_id = $adj->id;
                $unique_code = $adj->unique_code;
            }
            $appeal->giver_team_id = $team->id;
            $unique_code = $team->unique_code;
            $appeal->to_whom = $_POST['LanguageStatusAppeal']['to_whom'];
            $appeal->appeal = $_POST['LanguageStatusAppeal']['appeal'];

            if($_POST['unique_code'] === $unique_code){
                $appeal->team_id = $id;

                if($appeal->save())
                    Yii::$app->session->setFlash('success', 'Appeal submitted! Thank you!');
                    return $this->redirect(['index']);
            } else {
                Yii::$app->session->setFlash('error', 'Wrong Unique Code. Try Again!');
            }
        } else if (isset($_POST['giver']) && isset($_POST['unique_code']) && isset($_POST['LanguageStatusAppeal'])){
            Yii::$app->session->setFlash('error', 'Identify Yourself! Who are you?');
        }

        $teams = Team::find()->orderBy('team_name')->all();
        $adjs = Adjudicator::find()->orderBy('name')->all();

        return $this->render('appeal', ['model'=>$model, 'appeal'=> $appeal, 'teams'=>$teams, 'adjs'=>$adjs]);
    }

    public function actionApply($id)
    {
        $config = Yii::$app->params['config'];

        $model = $this->findTeam($id);
        if($model->languageStatus != null) throw new ForbiddenHttpException('You have registered! Contact admin if this is a mistake');
        $langstats = new LanguageStatus;
        $langstats->team_id = $model->id;

        

        if ($langstats->load(Yii::$app->request->post())) {
            $langstats->speaker_1_status = 'r';
            $langstats->speaker_1_interview = implode(',', $langstats->speaker_1_interview);

            $langstats->speaker_2_status = 'r';
            $langstats->speaker_2_interview = implode(',', $langstats->speaker_2_interview);

            $langstats->speaker_3_status = 'r';
            $langstats->speaker_3_interview = implode(',', $langstats->speaker_3_interview);
            
            if($langstats->save())
                return $this->redirect(['index']);
        } else {
            $statuses = explode(',', $config['language_status']);
            $statuses = array_combine($statuses, $statuses);
            
            if($config['language_status_interview_questions'] == '' || $config['language_status_interview_questions'] == null) $questions = [];
            else $questions = explode(';', $config['language_status_interview_questions']);

            return $this->render('apply', ['model'=>$model, 'langstats'=>$langstats, 'statuses'=>$statuses, 'questions'=>$questions]);
        }

        
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Team::find()->orderBy('team_name'),
            'pagination' => false,
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    private function findTeam($id){
        $team = Team::findOne($id);
        if($team == null) throw new NotFoundHttpException('Team Not Found');
        return $team;
    }

}
