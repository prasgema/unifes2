<?php

namespace frontend\controllers;

use frontend\models\Round;
use frontend\models\Debate;
use frontend\models\DebateAdj;
use yii\data\ActiveDataProvider;
use Yii;

class MatchupController extends \yii\web\Controller
{
	private function getLatestRound(){
        $round = Round::find()->where('status != \'p\'')->orderBy('round DESC')->one()->round; 
        return $round;
    }

    public function actionIndex($round = null, $team = null, $adj = null)
    {
    	$latestround = $this->getLatestRound();
    	if($round == null) $round = $latestround;

        $debate = Debate::find();
        $matchup = new ActiveDataProvider([
            'query' => DebateAdj::find()->leftJoin(['debate'=>$debate], 'debate.id = debate_id')->where(['round'=>$round])->orderBy('room, status'),
            'pagination'=>false,
        ]);

        return $this->render('index', [
        	'round' => $round,
        	'latestround' => $latestround,
            'matchup' => $matchup,
        ]);
    }

}
