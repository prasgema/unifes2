<?php

namespace frontend\controllers;
use frontend\models\Team;
use frontend\models\Adjudicator;
use yii\helpers\Html;
use Yii;

class RegoController extends \yii\web\Controller
{
    public function actionAdjudicator($id)
    {
        $model = Adjudicator::findOne($id);
        
        if ($model->load(Yii::$app->request->post())) {
            $model->generateUniqueCode();
            $model->status = 'l';
            $model->save(false);
            return $this->render('viewadj', ['model'=>$model, 'unique'=>'show']);
        } else {
            if($model->status == 'o') return $this->render('adj', ['model'=>$model]);
            else return $this->render('viewadj', ['model'=>$model, 'unique'=>'pras']);
        }
    }

    public function actionIndex()
    {
        if(isset($_POST['giver']) && $_POST['giver']!=null){
            print_r($_POST['giver']);

            $team = Team::find()->where(['team_name'=>$_POST['giver']])->one();
            if($team == null){
                $adj = Adjudicator::find()->where("CONCAT(name,' (',institution,')') = '{$_POST['giver']}'")->one();
                $this->redirect(['adjudicator','id'=>$adj->id]);
            }
            else $this->redirect(['team','id'=>$team->id]);

        } else {
            $teams = Team::find()->orderBy('team_name')->all();
            $adjs = Adjudicator::find()->orderBy('name')->all();

            $list = array();

            foreach($teams as $team) {
                $firstLetter = substr($team->team_name, 0, 1);
                if(!isset($list[$firstLetter])) $list[$firstLetter] = [ 'idx'=>$firstLetter, 'team'=>Html::a($team->team_name,['team','id'=>$team->id],['class'=>$team->status == 'l' ? 'btn btn-warning' : 'btn btn-default', 'disabled' => $team->status == 'l','style'=>'margin:1px']), 'adjudicator'=>''];
                else $list[$firstLetter]['team'].=Html::a($team->team_name,['team','id'=>$team->id],['class'=> $team->status == 'l' ? 'btn btn-warning' : 'btn btn-default', 'disabled' => $team->status == 'l', 'style'=>'margin:1px']);
            }
            foreach($adjs as $adj){
                $firstLetter = substr($adj->name, 0, 1);
                if(!isset($list[$firstLetter])) $list[$firstLetter] = [ 'idx'=>$firstLetter, 'team'=>'', 'adjudicator'=>Html::a($adj->name.' ('.$adj->institution.')',['adjudicator','id'=>$adj->id],['class'=>$adj->status == 'l' ? 'btn btn-warning' : 'btn btn-default', 'disabled' => $adj->status == 'l','style'=>'margin:1px'])];
                else $list[$firstLetter]['adjudicator'].=Html::a($adj->name.' ('.$adj->institution.')',['adjudicator','id'=>$adj->id],['class'=>$adj->status == 'l' ? 'btn btn-warning' : 'btn btn-default', 'disabled' => $adj->status == 'l','style'=>'margin:1px']);
            }

            ksort($list);

            return $this->render('index', [
                'teams' => $teams,
                'adjs' => $adjs,
                'list' => $list,
            ]);
        }
    }

    public function actionTeam($id)
    {
        $model = Team::findOne($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->generateUniqueCode();
            $model->status = 'l';
            $model->save(false);
            return $this->render('viewteam', ['model'=>$model, 'unique'=>'show']);
        } else {
            if($model->status == 'o') return $this->render('team', ['model'=>$model]);
            else return $this->render('viewteam', ['model'=>$model, 'unique'=>'pras']);
        }
    }
}
