<?php

namespace frontend\models;
use backend\models\Config;
use kartik\helpers\Html;

use Yii;

/**
 * This is the model class for table "adjudicator".
 *
 * @property integer $id
 * @property string $name
 * @property string $institution
 * @property integer $initial_score
 * @property string $unique_code
 *
 * @property DebateAdj[] $debateAdjs
 * @property Debate[] $debates
 * @property Feedback[] $feedbacks
 */
class Adjudicator extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'adjudicator';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name', 'institution', 'initial_score', 'unique_code'], 'required'],
            [['id', 'initial_score'], 'integer'],
            [['name'], 'string', 'max' => 30],
            [['institution'], 'string', 'max' => 20],
            [['unique_code'], 'string', 'max' => 3]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'institution' => 'Institution',
            'initial_score' => 'Initial Score',
            'unique_code' => 'Unique Code',
        ];
    }

    public function getNameInst()
    {
        return $this->name.' ('.$this->institution.')';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDebateAdjs()
    {
        return $this->hasMany(DebateAdj::className(), ['adjudicator_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDebates()
    {
        return $this->hasMany(Debate::className(), ['id' => 'debate_id'])->viaTable('debate_adj', ['adjudicator_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedbacks()
    {
        return $this->hasMany(Feedback::className(), ['giver_adj_id' => 'id']);
    }

    public function generateUniqueCode(){
        $this->unique_code = '000';

        do{
            $unique_code = rand()%899+100;

            $teams = Team::find()->where(['unique_code'=>$unique_code])->one();
            $adjs = Adjudicator::find()->where(['unique_code'=>$unique_code])->one();

            if(!$teams && !$adjs) $this->unique_code = $unique_code;
        }while($this->unique_code == '000');
    }

    public function getMissingFeedback(){
        $config = Yii::$app->params['config'];
        $sql = "SELECT COUNT(*) FROM debate_adj WHERE adj_id = {$this->id}";

        $sql_append = ' AND status IN (';
        if($config['chair_to'] != '?') $sql_append .= "'c',";
        if($config['panel_to'] != '?') $sql_append .= "'p',";
        if($config['trainee_to'] != '?') $sql_append .= "'t',";
        $sql_append .= "'z')";

        $sql_append .= " AND debate_id NOT IN (SELECT dac.debate_id FROM debate_adj dac WHERE dac.adj_id = {$this->id} AND (SELECT COUNT(*) FROM debate_adj dax WHERE dax.debate_id = dac.debate_id) = 1)";

        $utang = Yii::$app->db->createCommand($sql.$sql_append)->queryScalar();

        return $utang -  Yii::$app->db->createCommand("SELECT COUNT(DISTINCT round) FROM feedback WHERE giver_adj_id = {$this->id}")->queryScalar();
    }

    public function getDisplay(){
        $missing = $this->missingFeedback;
        if($missing > 0) return $this->nameInst.' '.Html::badge($this->missingFeedback);
        else return $this->nameInst;
    }

    public function import(){
        $config = Yii::$app->params['config'];

        if($config['system'] == 'tabbycat'){
            $connection = \Yii::$app->tabbycat;
     
            $model = $connection->createCommand('
                SELECT p.id "id", p.name "name", ai.code "institution", a.test_score "initial_score"
                FROM debate_adjudicator a,
                    debate_person p,
                    debate_institution ai
                WHERE a.person_ptr_id = p.id
                    AND a.institution_id = ai.id
            ');
        }
        else if($config['system'] == 'tabbie'){
            $connection = \Yii::$app->tabbie;

            $model = $connection->createCommand('
                SELECT `adjud_id` id,`adjud_name` name,`univ_code` institution,`ranking` initial_score
                FROM `adjudicator` a JOIN `university` u ON a.`univ_id`=u.`univ_id`
            ');
        }

        $result = $model->queryAll();
        
        return $result;
    }
}

