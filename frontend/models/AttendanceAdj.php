<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "attendance_adj".
 *
 * @property integer $adjudicator_id
 * @property integer $round
 * @property string $check_in
 *
 * @property Adjudicator $adjudicator
 * @property Round $round0
 */
class AttendanceAdj extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attendance_adj';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['adjudicator_id', 'round', 'check_in'], 'required'],
            [['adjudicator_id', 'round'], 'integer'],
            [['check_in'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'adjudicator_id' => 'Adjudicator ID',
            'round' => 'Round',
            'check_in' => 'Check In',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdjudicator()
    {
        return $this->hasOne(Adjudicator::className(), ['id' => 'adjudicator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRound0()
    {
        return $this->hasOne(Round::className(), ['round' => 'round']);
    }
}
