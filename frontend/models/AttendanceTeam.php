<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "attendance_team".
 *
 * @property integer $team_id
 * @property integer $round
 * @property string $check_in
 *
 * @property Team $team
 * @property Round $round0
 */
class AttendanceTeam extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attendance_team';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['team_id', 'round', 'check_in'], 'required'],
            [['team_id', 'round'], 'integer'],
            [['check_in'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'team_id' => 'Team ID',
            'round' => 'Round',
            'check_in' => 'Check In',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRound0()
    {
        return $this->hasOne(Round::className(), ['round' => 'round']);
    }
}
