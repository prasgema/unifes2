<?php

namespace frontend\models;
use yii\db\Query;

use Yii;

/**
 * This is the model class for table "debate".
 *
 * @property integer $id
 * @property integer $round
 * @property string $room
 * @property integer $team_id_og
 * @property integer $team_id_oo
 * @property integer $team_id_cg
 * @property integer $team_id_co
 *
 * @property Round $round0
 * @property Team $teamIdOg
 * @property Team $teamIdOo
 * @property Team $teamIdCg
 * @property Team $teamIdCo
 * @property DebateAdj[] $debateAdjs
 * @property Adjudicator[] $adjudicators
 */
class Debate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'debate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'round', 'room', 'team_id_og', 'team_id_oo'], 'required'],
            [['id', 'round', 'team_id_og', 'team_id_oo', 'team_id_cg', 'team_id_co'], 'integer'],
            [['room'], 'string', 'max' => 15]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'round' => 'Round',
            'room' => 'Room',
            'team_id_og' => 'Team Id Og',
            'team_id_oo' => 'Team Id Oo',
            'team_id_cg' => 'Team Id Cg',
            'team_id_co' => 'Team Id Co',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRound0()
    {
        return $this->hasOne(Round::className(), ['round' => 'round']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamOG()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id_og']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamOO()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id_oo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamCG()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id_cg']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamCO()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id_co']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDebateAdj()
    {
        return $this->hasMany(DebateAdj::className(), ['debate_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdjudicator()
    {
        return $this->hasMany(Adjudicator::className(), ['id' => 'adj_id'])->viaTable('debate_adj', ['debate_id' => 'id']);
    }

    public function getAllocation(){
        return '['.$this->round.'] '.$this->room.' - '.$this->teamOG->team_name.' - '.$this->teamOO->team_name.' - '.$this->teamCG->team_name.' - '.$this->teamCO->team_name;
    }

    public function populate(){
        return 'test';
    }
}
