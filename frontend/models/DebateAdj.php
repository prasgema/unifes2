<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "debate_adj".
 *
 * @property integer $debate_id
 * @property integer $adj_id
 * @property string $status
 *
 * @property Debate $debate
 * @property Adjudicator $adj
 */
class DebateAdj extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'debate_adj';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['debate_id', 'adj_id', 'status'], 'required'],
            [['debate_id', 'adj_id'], 'integer'],
            [['status'], 'string', 'max' => 1],
            [['debate_id', 'adj_id'], 'unique', 'targetAttribute' => ['debate_id', 'adj_id'], 'message' => 'The combination of Debate ID and Adj ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'debate_id' => 'Debate ID',
            'adj_id' => 'Adj ID',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDebate()
    {
        return $this->hasOne(Debate::className(), ['id' => 'debate_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdjudicator()
    {
        return $this->hasOne(Adjudicator::className(), ['id' => 'adj_id']);
    }

    public function getAdjNameInst()
    {
        $adj = $this->hasOne(Adjudicator::className(), ['id' => 'adj_id']);
        return $adj->one()->nameInst;
    }
}
