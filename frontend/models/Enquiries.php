<?php

namespace frontend\models;
use kartik\helpers\Html;

use Yii;

/**
 * This is the model class for table "enquiries".
 *
 * @property integer $id
 * @property integer $round
 * @property integer $adj_id
 * @property integer $giver_team_id
 * @property integer $giver_adj_id
 * @property string $giver_status
 * @property string $agree_initial
 * @property string $agree_eventual
 * @property string $influence
 * @property string $comments
 * @property string $status
 * @property string $created_at
 *
 * @property Round $round0
 * @property Adjudicator $adj
 * @property Team $giverTeam
 * @property Adjudicator $giverAdj
 * @property EnquiriesDetail[] $enquiriesDetails
 * @property Question[] $questions
 * @property EnquiriesTag[] $enquiriesTags
 */
class Enquiries extends \yii\db\ActiveRecord
{
    public $tag;
    public $influences;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'enquiries';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['round', 'room', 'adj_id', 'status','influences','tag', 'created_at'], 'required'],
            [['round', 'adj_id', 'giver_team_id', 'giver_adj_id'], 'integer'],
            [['comments'], 'string'],
            [['created_at'], 'safe'],
            [['giver_status'], 'string', 'max' => 2],
            [['agree_initial', 'agree_eventual', 'status'], 'string', 'max' => 1],
            [['influence'], 'string', 'max' => 3]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'round' => 'Round',
            'adj_id' => 'Adj ID',
            'giver_team_id' => 'Giver Team ID',
            'giver_adj_id' => 'Giver Adj ID',
            'giver_status' => 'Giver Status',
            'agree_initial' => 'Agree Initial',
            'agree_eventual' => 'Agree Eventual',
            'influence' => 'Influence',
            'comments' => 'Comments',
            'status' => 'Status',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRound0()
    {
        return $this->hasOne(Round::className(), ['round' => 'round']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdj()
    {
        return $this->hasOne(Adjudicator::className(), ['id' => 'adj_id']);
    }

    public function getAdjName()
    {
        return $this->hasOne(Adjudicator::className(), ['id' => 'adj_id'])->one()->name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGiverTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'giver_team_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGiverAdj()
    {
        return $this->hasOne(Adjudicator::className(), ['id' => 'giver_adj_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEnquiriesDetails()
    {
        return $this->hasMany(EnquiriesDetail::className(), ['enquiries_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions()
    {
        return $this->hasMany(Question::className(), ['id' => 'question_id'])->viaTable('enquiries_detail', ['enquiries_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEnquiriesTags()
    {
        return $this->hasMany(EnquiriesTag::className(), ['enquiries_id' => 'id']);
    }

    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])->viaTable('enquiries_tag', ['enquiries_id' => 'id']);
    }

    public function getGiverStatus(){
        switch($this->giver_status){
            case 'c' : return 'Chair Adjudicator';
            case 'p' : return 'Panelist Judge';
            case 't' : return 'Trainee Adjudicator';
            case 'og' : return 'Opening Government';
            case 'oo' : return 'Opening Opposition';
            case 'cg' : return 'Closing Government';
            case 'co' : return 'Closing Opposition';
        }
    }

    public function getAgree(){
        if($this->agree_initial == 'y' && $this->agree_eventual == 'y') return Html::bsLabel('Initially and Eventually');
        else if($this->agree_initial == 'y') return Html::bsLabel('Initially');
        else if($this->agree_eventual == 'y') return Html::bsLabel('Eventually');
        else return Html::bsLabel('NOPE!');
    }

    public function getFeedbackInfluence(){
        $result = '';

        if(strstr($this->influence, 'r')) $result.=(Html::bsLabel('Result').' ');
        if(strstr($this->influence, 'e')) $result.=(Html::bsLabel('Explanation').' ');
        if(strstr($this->influence, 's')) $result.=(Html::bsLabel('Score').' ');
        if(strstr($this->influence, 'f')) $result.=(Html::bsLabel('Individual Feedback').' ');
        
        return $result;
    }

    public function getTagging(){
        $tags = $this->tags;
        $result = '';
        foreach($tags as $t){
            $result.=(Html::bsLabel($t->tag).' ');
        }
        return $result;
    }

    public function getAverageScore()
    {
        $feedback = $this->hasMany(EnquiriesDetail::className(), ['enquiries_id' => 'id'])->all();
        $sum = 0; $count = 0;

        foreach($feedback as $f){
            $sum+=$f->score;
            $count++;
        }
        return $sum/$count;
    }

    public function getScores()
    {
        $feedback = $this->hasMany(EnquiriesDetail::className(), ['enquiries_id' => 'id'])->all();
        $scores = '';
        foreach($feedback as $f) $scores .= Html::bsLabel($f->question->question_label.' : '.$f->score).' ';

        return $scores;
    }

    public function getGiverAdjName()
    {
        $giver = $this->hasOne(Adjudicator::className(), ['id' => 'giver_adj_id'])->one();
        if($giver != null) return $giver->name;
        else return '';
    }

    public function getGiverTeamName()
    {
        $giver = $this->hasOne(Team::className(), ['id' => 'giver_team_id'])->one();
        if($giver != null) return $giver->team_name;
        else return '';
    }

    public function getGiver()
    {
        return $this->giverAdjName.$this->giverTeamName;
    }
}
