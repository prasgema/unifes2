<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "enquiries_detail".
 *
 * @property integer $enquiries_id
 * @property integer $question_id
 * @property integer $score
 *
 * @property Question $question
 * @property Enquiries $enquiries
 */
class EnquiriesDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'enquiries_detail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['enquiries_id', 'question_id', 'score'], 'required'],
            [['enquiries_id', 'question_id', 'score'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'enquiries_id' => 'Enquiries ID',
            'question_id' => 'Question ID',
            'score' => 'Score',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(Question::className(), ['id' => 'question_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEnquiries()
    {
        return $this->hasOne(Enquiries::className(), ['id' => 'enquiries_id']);
    }
}
