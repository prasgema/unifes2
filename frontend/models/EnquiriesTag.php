<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "enquiries_tag".
 *
 * @property integer $enquiries_id
 * @property integer $tag_id
 *
 * @property Enquiries $tag
 * @property Enquiries $enquiries
 */
class EnquiriesTag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'enquiries_tag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['enquiries_id', 'tag_id'], 'required'],
            [['enquiries_id', 'tag_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'enquiries_id' => 'Enquiries ID',
            'tag_id' => 'Tag ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTag()
    {
        return $this->hasOne(Enquiries::className(), ['id' => 'tag_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEnquiries()
    {
        return $this->hasOne(Enquiries::className(), ['id' => 'enquiries_id']);
    }
}
