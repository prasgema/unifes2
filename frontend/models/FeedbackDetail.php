<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "feedback_detail".
 *
 * @property integer $feedback_id
 * @property integer $question_id
 * @property integer $score
 *
 * @property Feedback $feedback
 * @property Question $question
 */
class FeedbackDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'feedback_detail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['feedback_id', 'question_id', 'score'], 'required'],
            [['feedback_id', 'question_id', 'score'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'feedback_id' => 'Feedback ID',
            'question_id' => 'Question ID',
            'score' => 'Score',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedback()
    {
        return $this->hasOne(Feedback::className(), ['id' => 'feedback_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(Question::className(), ['id' => 'question_id']);
    }
}
