<?php

namespace frontend\models;

use Yii;
use kartik\helpers\Html;

/**
 * This is the model class for table "language_status".
 *
 * @property integer $team_id
 * @property string $speaker_1_applying_as
 * @property string $speaker_1_status
 * @property string $speaker_1_interview
 * @property string $speaker_2_applying_as
 * @property string $speaker_2_status
 * @property string $speaker_2_interview
 * @property string $speaker_3_applying_as
 * @property string $speaker_3_status
 * @property string $speaker_3_interview
 *
 * @property Team $team
 * @property LanguageStatusAppeal[] $languageStatusAppeals
 */
class LanguageStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'language_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['team_id'], 'required'],
            [['team_id'], 'integer'],
            [['speaker_1_applying_as', 'speaker_2_applying_as', 'speaker_3_applying_as'], 'string', 'max' => 10],
            [['speaker_1_status', 'speaker_2_status', 'speaker_3_status'], 'string', 'max' => 1],
            [['speaker_1_interview', 'speaker_2_interview', 'speaker_3_interview'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'team_id' => 'Team ID',
            'speaker_1_applying_as' => '#1 would like to be considered as',
            'speaker_1_status' => 'Speaker 1 Status',
            'speaker_1_interview' => 'Speaker 1 Interview',
            'speaker_2_applying_as' => '#2 would like to be considered as',
            'speaker_2_status' => 'Speaker 2 Status',
            'speaker_2_interview' => 'Speaker 2 Interview',
            'speaker_3_applying_as' => '#3 would like to be considered as',
            'speaker_3_status' => 'Speaker 3 Status',
            'speaker_3_interview' => 'Speaker 3 Interview',
            'd1s' => '',
            'd2s' => '',
            'd3s' => ''
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppeals()
    {
        return $this->hasMany(LanguageStatusAppeal::className(), ['team_id' => 'team_id']);
    }

    private function display($language, $status){
        $color = Html::TYPE_DEFAULT;
        if($language == 'efl') $color = Html::TYPE_SUCCESS;
        else if($language == 'esl') $color = Html::TYPE_PRIMARY;
        else if($language == 'novice') $color = Html::TYPE_INFO;

        $statuscolor = Html::TYPE_DEFAULT;
        if($status == 'a') $statuscolor = Html::TYPE_SUCCESS;
        else if($status == 'i') $statuscolor = Html::TYPE_WARNING;
        else if($status == 'r') $statuscolor = Html::TYPE_DANGER;

        return Html::bsLabel($language, $color) . ' ' . Html::bsLabel($status, $statuscolor);
    }

    public function getD1s(){
        return $this->display($this->speaker_1_applying_as, $this->speaker_1_status);
    }

    public function getD2s(){
        return $this->display($this->speaker_2_applying_as, $this->speaker_2_status);
    }

    public function getD3s(){
        return $this->display($this->speaker_3_applying_as, $this->speaker_3_status);
    }

    public function getAppealed()
    {
        return Html::bsLabel(count($this->appeals));
    }
}
