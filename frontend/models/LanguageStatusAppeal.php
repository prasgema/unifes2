<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "language_status_appeal".
 *
 * @property integer $appeal_id
 * @property integer $giver_team_id
 * @property integer $giver_adj_id
 * @property integer $team_id
 * @property string $to_whom
 * @property string $appeal
 *
 * @property Team $giverTeam
 * @property Adjudicator $giverAdj
 * @property LanguageStatus $team
 */
class LanguageStatusAppeal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'language_status_appeal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['giver_team_id', 'giver_adj_id', 'team_id'], 'integer'],
            [['team_id', 'to_whom', 'appeal'], 'required'],
            [['appeal'], 'string'],
            [['to_whom'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'appeal_id' => 'Appeal ID',
            'giver_team_id' => 'Giver Team ID',
            'giver_adj_id' => 'Giver Adj ID',
            'team_id' => 'Team ID',
            'to_whom' => 'To Whom',
            'appeal' => 'Appeal',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGiverTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'giver_team_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGiverAdj()
    {
        return $this->hasOne(Adjudicator::className(), ['id' => 'giver_adj_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(LanguageStatus::className(), ['team_id' => 'team_id']);
    }
}
