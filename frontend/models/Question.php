<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "question".
 *
 * @property integer $id
 * @property string $question_label
 * @property string $question
 *
 * @property FeedbackDetail[] $feedbackDetails
 * @property Feedback[] $feedbacks
 */
class Question extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'question';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['question_label'], 'string', 'max' => 10],
            [['question'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question_label' => 'Question Label',
            'question' => 'Question',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedbackDetails()
    {
        return $this->hasMany(FeedbackDetail::className(), ['question_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedbacks()
    {
        return $this->hasMany(Feedback::className(), ['id' => 'feedback_id'])->viaTable('feedback_detail', ['question_id' => 'id']);
    }
}
