<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "round".
 *
 * @property integer $round
 * @property string $status
 * @property string $start_at
 * @property string $motion
 *
 * @property DebateTeam[] $debateTeams
 * @property Feedback[] $feedbacks
 */
class Round extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'round';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['start_at'], 'safe'],
            [['motion'], 'string'],
            [['status'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'round' => 'Round',
            'status' => 'Status',
            'start_at' => 'Start At',
            'motion' => 'Motion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDebateTeams()
    {
        return $this->hasMany(DebateTeam::className(), ['round' => 'round']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedbacks()
    {
        return $this->hasMany(Feedback::className(), ['round' => 'round']);
    }

    public function import($round){
        $config = Yii::$app->params['config'];

        if($config['system'] == 'tabbycat'){
            $connection = \Yii::$app->tabbycat;
     
            $model1 = $connection->createCommand("
                SELECT d.id id, d.round_id round, v.name room, t1.team_id team_id_og, t2.team_id team_id_oo, '' team_id_cg, '' team_id_co
                FROM (((debate_debate d JOIN debate_debateteam t1 ON d.id=t1.debate_id)
                    JOIN debate_debateteam t2 ON d.id=t2.debate_id)
                    JOIN debate_venue v ON v.id=d.venue_id)
                WHERE t1.position='A' AND t2.position='N' AND d.round_id={$round}
            ");
            $model2 = $connection->createCommand("
                SELECT d.id debate_id, adjudicator_id adj_id, LOWER(type) status
                FROM debate_debateadjudicator a JOIN debate_debate d ON d.id=a.debate_id
                WHERE d.round_id = {$round}
            ");
        }
        else if($config['system'] == 'tabbie'){
            $connection = \Yii::$app->tabbie;

            $model1 = $connection->createCommand("
                SELECT `debate_id` id, '{$round}' round, `venue_name` room, `og` team_id_og, `oo` team_id_oo, `cg` team_id_cg, `co` team_id_co
                FROM `draw_round_{$round}` d JOIN `venue` v ON v.venue_id = d.venue_id
            ");

            $model2 = $connection->createCommand("
                SELECT debate_id, adjud_id adj_id, SUBSTR(`status`, 1,1) `status` FROM `adjud_round_{$round}`
            ");
        }

        $result['debate'] = $model1->queryAll();
        $result['debateAdj'] = $model2->queryAll();
        
        return $result;
    }

    public function getProgressAdj(){
        $config = Yii::$app->params['config'];
        $sql = "SELECT COUNT(*) FROM debate_adj da JOIN debate d ON d.id=da.debate_id WHERE round = {$this->round}";

        $sql_append = ' AND status IN (';
        if($config['chair_to'] != '?') $sql_append .= "'c',";
        if($config['panel_to'] != '?') $sql_append .= "'p',";
        if($config['trainee_to'] != '?') $sql_append .= "'t',";
        $sql_append .= "'z')";

        $adj = Yii::$app->db->createCommand($sql.$sql_append)->queryScalar();
        $feed = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT round) FROM feedback WHERE round = {$this->round} AND giver_adj_id IS NOT NULL")->queryScalar();

        return $adj==0 ? 0 : 100*$feed/$adj;
    }

    public function getProgressTeam(){
        $config = Yii::$app->params['config'];
        $debate = Yii::$app->db->createCommand("SELECT COUNT(*) FROM debate WHERE round={$this->round}")->queryScalar();
        $team = (($config['system'] == 'tabbycat') ? $debate*2 : (($config['system'] == '3tab') ? $debate*2 : $debate*4));
        $feed = Yii::$app->db->createCommand("SELECT COUNT(DISTINCT round) FROM feedback WHERE round = {$this->round} AND giver_team_id IS NOT NULL")->queryScalar();

        return $team==0 ? 0 : 100*$feed/$team;
    }

    public function getPrev(){
        return $this->findOne($this->round-1);
    }

    public function getNext(){
        return $this->findOne($this->round+1);
    }
}
