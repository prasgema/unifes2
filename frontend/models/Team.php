<?php

namespace frontend\models;
use kartik\helpers\Html;

use Yii;

/**
 * This is the model class for table "team".
 *
 * @property integer $id
 * @property string $team_name
 * @property string $debater_1
 * @property string $debater_2
 * @property string $debater_3
 * @property string $unique_code
 *
 * @property Debate[] $debates
 * @property Feedback[] $feedbacks
 */
class Team extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'team';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'team_name', 'debater_1', 'debater_2', 'unique_code'], 'required'],
            [['id'], 'integer'],
            [['team_name', 'debater_1', 'debater_2', 'debater_3'], 'string', 'max' => 20],
            [['unique_code'], 'string', 'max' => 3]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'team_name' => 'Team Name',
            'debater_1' => 'Debater 1',
            'debater_2' => 'Debater 2',
            'debater_3' => 'Debater 3',
            'unique_code' => 'Unique Code',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDebates()
    {
        return $this->hasMany(Debate::className(), ['team_id_co' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedbacks()
    {
        return $this->hasMany(Feedback::className(), ['giver_team_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguageStatus()
    {
        return $this->hasOne(LanguageStatus::className(), ['team_id' => 'id']);
    }

    public function generateUniqueCode(){
        $this->unique_code = '000';

        do{
            $unique_code = rand()%899+100;

            $teams = Team::find()->where(['unique_code'=>$unique_code])->one();
            $adjs = Adjudicator::find()->where(['unique_code'=>$unique_code])->one();

            if(!$teams && !$adjs) $this->unique_code = $unique_code;
        }while($this->unique_code == '000');
    }


    public function getMissingFeedback(){
        return Yii::$app->db->createCommand("SELECT COUNT(*) FROM debate WHERE team_id_og = {$this->id} OR team_id_oo = {$this->id} OR team_id_cg = {$this->id} OR team_id_co = {$this->id}")->queryScalar() -  Yii::$app->db->createCommand("SELECT COUNT(DISTINCT round.round) FROM feedback RIGHT JOIN round ON (feedback.round = round.round) WHERE giver_team_id = {$this->id} OR round.status = 's'")->queryScalar();
    }

    public function getDisplay(){
        $missing = $this->missingFeedback;
        if($missing > 0) return $this->team_name.' '.Html::badge($missing);
        else return $this->team_name;
    }

    public function import(){
        $config = Yii::$app->params['config'];

        if($config['system'] == 'tabbycat'){
            $connection = \Yii::$app->tabbycat;
     
            $model = $connection->createCommand('
                SELECT t.id "id", CONCAT(i.code,\' \',t.reference) "team_name", p1.name "debater_1", p2.name "debater_2", p3.name "debater_3"
                FROM (((((((debate_team t JOIN debate_institution i ON t.institution_id=i.id)
                    JOIN debate_speaker s1 ON s1.team_id=t.id) JOIN debate_person p1 ON s1.person_ptr_id=p1.id)
                    JOIN debate_speaker s2 ON s2.team_id=t.id) JOIN debate_person p2 ON s2.person_ptr_id=p2.id)
                    JOIN debate_speaker s3 ON s3.team_id=t.id) JOIN debate_person p3 ON s3.person_ptr_id=p3.id)
                WHERE p1.id < p2.id AND p2.id < p3.id
            ');
        }
        else if($config['system'] == 'tabbie'){
            $connection = \Yii::$app->tabbie;

            $model = $connection->createCommand('
                SELECT sp1.`team_id` id, CONCAT(u.`univ_code`,\' \',t.`team_code`) team_name, sp1.`speaker_name` debater_1, sp2.`speaker_name` debater_2, \'\' debater_3
                FROM (((`speaker` sp1 JOIN `speaker` sp2 ON sp1.`team_id`=sp2.`team_id`)
                    JOIN `team` t ON sp1.`team_id`=t.`team_id`) 
                    JOIN `university` u ON t.`univ_id`=u.`univ_id`)
                WHERE sp1.`speaker_id` > sp2.`speaker_id`
            ');
        }

        $result = $model->queryAll();

        return $result;
    }
}
