<?php

use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
?>
<script>
window.setTimeout(function(){
    // Move to a new location or you can do something else
    window.location.href = "<?=Url::previous() ?>";
}, 2000);
</script>
<h1 style="color:red;">CHECK IN FAILED!</h1>

<p>
	You might have checked in before, or you entered a wrong unique code
    <?=Html::a('[back to front page]', ['index'], ['class'=>'btn btn-primary']) ?>
</p>
