<?php
use kartik\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Attendance';
?>

<script type="text/javascript">
	function number_write(x)
	{
		var text_box = document.getElementById("code");
		if(x>=0 && x<=9)
		{
			if(isNaN(text_box.value))
				text_box.value = 0;
			text_box.value = text_box.value.toString() + x.toString();
		}
	}
	function number_clear()
	{
		document.getElementById("code").value = '';
	}
</script>
<style type="text/css">
	.main_panel
	{
		overflow:hidden;
		background-color:#f5f5f5;
		padding:5px;
	}
	.number_button
	{
		width:50px;
		height:50px;
		margin:10px;
		float:left;
		background-color:#ddd;
		border: 1px solid white;
		border-top-right-radius:25px;
		border-top-left-radius:25px;
		border-bottom-right-radius:25px;
		border-bottom-left-radius:25px;
		font-size:36px;
		text-align:center;
	}
	.number_button:hover
	{
		background-color:#fff;
	}
	.text_box
	{
		width:250px; 
		height:30px;
		font-size:24px;
		text-align:right;
	}
</style>

<div class="attendance-index">
    <h1>ATTENDANCE CHECK IN FOR ROUND <?= $round ?></h1>

    <div class="jumbotron" style="text-align:center; overflow:hidden;">
        <?php $form = ActiveForm::begin(); ?>
            <h1><?= Html::input('password','code','',['class'=>'form-control', 'id'=>'code', 'style'=>'font-size:75%; height:150%; text-align:center;', 'placeholder'=>'Your Unique Code']) ?></h1>
            
            <div class="main_panel hidden-lg">
				<div class="number_button" onclick="number_write(1);">1</div>
				<div class="number_button" onclick="number_write(2);">2</div>
				<div class="number_button" onclick="number_write(3);">3</div><br/>
				<div class="number_button" onclick="number_write(4);">4</div>
				<div class="number_button" onclick="number_write(5);">5</div>
				<div class="number_button" onclick="number_write(6);">6</div><br/>
				<div class="number_button" onclick="number_write(7);">7</div>
				<div class="number_button" onclick="number_write(8);">8</div>
				<div class="number_button" onclick="number_write(9);">9</div><br/>
				<div class="number_button" onclick="number_clear();">C</div>
				<div class="number_button" onclick="number_write(0);">0</div>
			</div>

            <?= Html::submitButton('CONFIRM ATTENDANCE', ['class'=>'btn btn-primary']); ?>
        <?php ActiveForm::end(); ?>
    </div><br/>
</div>