<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveField;

/* @var $this yii\web\View */
/* @var $model app\models\Feedback */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="feedback-form">
    
    <h2>
    <?php
        for($x=1; $x<=$latestRound; $x++)
            echo Html::a('R'.$x,['create', 'method'=>$method, 'giver'=>$giver, 'round'=>$x],['disabled'=> $x==$round , 'style'=>'margin:0.5%; width: '.((100/$latestRound)-1).'% ;', 'class'=> $x==$round ? 'btn btn-primary' : (in_array( ['round'=>$x], $feedback_done ) ? 'btn btn-warning' : 'btn btn-default') ])
    ?></h2><br/><br/>

    <?php $form = ActiveForm::begin(); ?>

    <?php
        $adj = $debate->debateAdj;
        foreach($adj as $i => $a){
            if($a->adj_id != $giver || $method == 'd2a'){
                echo '<h2>'.$a->adjudicator->name.'</h2>';

                if($method == 'a2a') echo $form->field($model, "[{$i}]agree_initial")->RadioList(['y' => 'Yes', 'n'=>'No'], ['separator'=>'&emsp;&emsp;', 'itemOptions' => ['class' =>'radio-inline', 'style'=>'margin:0 5px;']])->label('Do you agree with the initial decision of the adjudicator?');
        
                echo $form->field($model, "[{$i}]agree_eventual")->RadioList(['y' => 'Yes', 'n'=>'No'], ['separator'=>'&emsp;&emsp;', 'itemOptions' => ['class' =>'radio-inline', 'style'=>'margin:0 5px;']])->label('Do you agree with the result of the adjudication?');

                foreach($question as $j => $q) echo $form->field($d_mdl, "[{$i}][{$j}]score")->radioList([1=>1,2=>2,3=>3,4=>4,5=>5], ['separator'=>'&emsp;&emsp;', 'itemOptions' => ['class' =>'radio-inline', 'style'=>'margin:0 5px;']])->label($q->question);
                
                if($method == 'a2a') echo $form->field($model, "[{$i}]influences")->checkboxList(['r'=>'Result','e'=>'Explanation','s'=>'score'], ['separator'=>'&emsp;&emsp;', 'itemOptions' => ['class' =>'radio-inline', 'style'=>'margin:0 5px;']])->label('What Influences your feedback the most?');
                else if($method == 'd2a') echo $form->field($model, "[{$i}]influences")->checkboxList(['r'=>'Result','e'=>'Explanation','f'=>'Feedback'], ['separator'=>'&emsp;&emsp;', 'itemOptions' => ['class' =>'radio-inline', 'style'=>'margin:0 5px;']])->label('What Influences your feedback the most?');

                echo $form->field($model, "[{$i}]tag")->checkboxList(ArrayHelper::map($tag, 'id', 'tag'), ['separator'=>'&emsp;&emsp;', 'itemOptions' => ['class' =>'radio-inline', 'style'=>'margin:0 5px;']])->label($q->question);;
            
                echo $form->field($model, "[{$i}]comments")->textarea(['rows' => 6]);

                echo '<br/><hr/><br/>';
            }
        }
    ?>

    <div class="form-group">
        <?= Html::submitButton('Submit and Fill Another Feedback', ['class' => 'btn btn-primary', 'name'=>'submit']) ?>
        <?= Html::submitButton('Submit and Finish Filling All Feedback', ['class' => 'btn btn-warning', 'name'=>'end']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
