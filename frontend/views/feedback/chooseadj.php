<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveField;
use kartik\widgets\ActiveForm;
use kartik\builder\TabularForm;
use kartik\grid\GridView;
use kartik\widgets\Select2;
use yii\data\ArrayDataProvider;

for($x=0; $x<$panels; $x++) $data[] = ['adj_id'=>NULL];
$dataProvider = new ArrayDataProvider(['allModels'=>$data, 'pagination'=>false]);

/* @var $this yii\web\View */
/* @var $model app\models\Feedback */

$this->title = 'Choose Adjudicators to be Given Feedback';
?>
<div class="feedback-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <h3>If adjudicator is not present in the list, please notify administrator/tab team AND fill in the paper feedback</h3>

	<div class="feedback-form">
	    <?php $form = ActiveForm::begin(); ?>

		<?= TabularForm::widget([
		    // your data provider
		    'dataProvider'=>$dataProvider,
		 
		    // formName is mandatory for non active forms
		    // you can get all attributes in your controller 
		    // using $_POST['kvTabForm']
		    'formName'=>'adjudicator',
		    'actionColumn'=>false,
		    'checkboxColumn'=>false,
		    
		    // set defaults for rendering your attributes
		    'attributeDefaults'=>[
		        'type'=>TabularForm::INPUT_TEXT,
		    ],
		    
		    // configure attributes to display
		    'attributes'=>[
		        'adj_id'=>[
		        	'label'=>'Adjudicator',
		        	'type'=>TabularForm::INPUT_WIDGET, 
		        	'widgetClass'=>Select2::classname(),
	        		'options' => [
	        			'data'=>ArrayHelper::map($adjudicator, 'id', 'nameInst'),
	        			'options'=> ['placeholder' => 'Select Adjudicator',],
		                'pluginOptions' => [
		                    'allowClear' => true,
		                ],
	        		],
		        ],
		    ],
		    
		    // configure other gridview settings
		    'gridSettings'=>[
		        'panel'=>[
		            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-user"></i> Edit Panel Adjudicators</h3>',
		            'type'=>GridView::TYPE_PRIMARY,
		            'before'=> Html::a('<i class="glyphicon glyphicon-plus"></i> Add Another Panel Adjudicators', ['enquiries','method' => $method, 'giver' => $giver, 'panels'=>$panels+1], ['class'=>'btn btn-success']) . ' ' . 
		                Html::a('<i class="glyphicon glyphicon-remove"></i> Reset Form', ['enquiries','method' => $method, 'giver' => $giver, 'panels'=>1], ['class'=>'btn btn-danger kv-batch-delete']),
		            'footer'=>false,
		            'after'=> Html::submitButton('<i class="glyphicon glyphicon-ok-sign"></i> Fill Feedback Now!', ['class'=>'btn btn-primary kv-batch-save']).' '.
		            	Html::a('Back to Front Page', ['index'], ['class'=>'btn btn-default'])
		        ],
		    ]
		]) ?>

	    <?php ActiveForm::end(); ?>

	</div>


</div>