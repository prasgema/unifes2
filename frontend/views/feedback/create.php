<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveField;


/* @var $this yii\web\View */
/* @var $model app\models\Feedback */

$this->title = 'Feedback Submission';
?>
<script>
	function panelverbal() {
		var fields = document.getElementsByName("judges");
		var count = fields.length;

		for(var x=0; x<count; x++) fields[x].style.display = "block"; 
	}
	function notAttending(x,adjname) {
		var container = document.getElementById("place"+x);
		container.innerHTML = (adjname+" did not attend the round");
	}
</script>

<div class="feedback-create">

    <h1><?= Html::encode($this->title) ?></h1>

	<div class="feedback-form">
	    
	    <h2>
	    <?php
	        for($x=1; $x<=$latestRound; $x++)
	            echo Html::a('R'.$x,['create', 'method'=>$method, 'giver'=>$giver, 'round'=>$x],['disabled'=> $x==$round , 'style'=>'margin:0.5%; width: '.((100/$latestRound)-1).'% ;', 'class'=> $x==$round ? 'btn btn-primary' : (in_array( ['round'=>$x], $feedback_done ) ? 'btn btn-warning' : 'btn btn-default') ])
	    ?></h2><br/><br/>

	    <?php $form = ActiveForm::begin(); ?>

	    <p>
	    	Any problem? 
	    	<?= $config['team_to']=='c' && $method == 'd2a'? Html::button('Panel did the verbal, not the chair', ['class'=>'btn btn-default', 'onclick'=>'panelverbal()']) : '' ?>
	    	<?= Html::a('Wrong/Switched judge(s)', ['enquiries', 'method'=>$method, 'giver'=>$giver], ['class'=>'btn btn-default']) ?>
	    	<?= Html::a('I need to reset my form', ['create', 'method'=>$method, 'giver'=>$giver, 'round'=>$round], ['class'=>'btn btn-default']) ?>
	    </p>
	    <?php
	        $adj = $debate->debateAdj;
	        $giver_stats = 't';
	        $giver_gives = '?';
	        foreach($adj as $i => $a) if($a->adj_id == $giver) $giver_stats = $a->status; //dia siapa. ini lebih cepet daripada perlu pass variable lagi dari controller
	        switch($giver_stats){ //cari tau dia bisa ngasi siapa aja
	        	case 'c' : $giver_gives = $config['chair_to']; break;
	        	case 'p' : $giver_gives = $config['panel_to']; break;
	        	default : $giver_gives = $config['trainee_to']; break;
	        }

	        foreach($adj as $i => $a){
	        	$valid = false;

	        	if($method == 'd2a' && (($config['team_to']=='c' && $a->status=='c') || $config['team_to']=='*')) $valid = true;
	        	if($method == 'a2a' && $a->adj_id != $giver){
	        		if($giver_gives == '*') $valid = true;
	        		else if($giver_gives == $a->status) $valid = true;
	        		else if($giver_gives == 'p' && $a->status == 't') $valid = true;
	        	}

	            if($valid){
	            	echo '<div id="place'.$i.'" style="margin-bottom:10px; background:#eee; padding:5px 20px;">';

	                if($method == 'a2a') echo '<h2>'.$a->adjudicator->name.' '.Html::button('Not Attending',['class'=>'btn btn-default', 'onclick'=>"notAttending({$i}, '{$a->adjudicator->name}')"]).'</h2>';
	                else if($method == 'd2a') echo '<h2>'.$a->adjudicator->name.'</h2>';

	                $model->adj_id = $a->adj_id;
	                echo $form->field($model, "[{$i}]adj_id", ['options'=>['name'=>'judges','style'=>'display:none;']])->dropdownList(ArrayHelper::map($adj, 'adj_id', 'adjNameInst'))->label('Who deliver the verbal?');
	                
	                if($method == 'a2a') echo $form->field($model, "[{$i}]agree_initial")->RadioList(['y' => 'Yes', 'n'=>'No'], ['separator'=>'&emsp;&emsp;', 'itemOptions' => ['class' =>'radio-inline', 'style'=>'margin:0 5px;']])->label('Do you agree with the initial decision of the adjudicator?');
	        
	                echo $form->field($model, "[{$i}]agree_eventual")->RadioList(['y' => 'Yes', 'n'=>'No'], ['separator'=>'&emsp;&emsp;', 'itemOptions' => ['class' =>'radio-inline', 'style'=>'margin:0 5px;']])->label('Do you agree with the result of the adjudication?');

	                foreach($question as $j => $q) echo $form->field($d_mdl, "[{$i}][{$j}]score")->radioList(array_combine(range(1,$config['max_score']),range(1,$config['max_score'])), ['separator'=>'&emsp;&emsp;', 'itemOptions' => ['class' =>'radio-inline', 'style'=>'margin:0 5px;']])->label($q->question);
	                
	                if($method == 'a2a') echo $form->field($model, "[{$i}]influences")->checkboxList(['r'=>'Result','e'=>'Explanation','s'=>'Score'], ['separator'=>'&emsp;&emsp;', 'itemOptions' => ['class' =>'radio-inline', 'style'=>'margin:0 5px;']])->label('What Influences your feedback the most?');
	                else if($method == 'd2a') echo $form->field($model, "[{$i}]influences")->checkboxList(['r'=>'Result','e'=>'Explanation','f'=>'Individual Feedback'], ['separator'=>'&emsp;&emsp;', 'itemOptions' => ['class' =>'radio-inline', 'style'=>'margin:0 5px;']])->label('What Influences your feedback the most?');

	                if($config['tagging'] == 'y') echo $form->field($model, "[{$i}]tag")->checkboxList(ArrayHelper::map($tag, 'id', 'tag'), ['separator'=>'&emsp;&emsp;', 'itemOptions' => ['class' =>'radio-inline', 'style'=>'margin:0 5px;']]);
	            
	                if($config['comment'] == 'y') echo $form->field($model, "[{$i}]comments")->textarea(['rows' => 6]);
	                echo '</div>';
	            }
	        }
	    ?>

	    <div class="form-group">
	        <?= Html::submitButton('Submit and Fill Another Feedback', ['class' => 'btn btn-primary', 'name'=>'submit']) ?>
	        <?= Html::submitButton('Submit and Finish Filling All Feedback', ['class' => 'btn btn-warning', 'name'=>'end']) ?>
	        <?= Html::a('Back to Front Page', ['index'], ['class' => 'btn btn-default', 'name'=>'end']) ?>
	    </div>

	    <?php ActiveForm::end(); ?>

	</div>


</div>
