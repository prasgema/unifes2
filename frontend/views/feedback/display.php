<?php
use yii\helpers\Html;
use yii\bootstrap\Progress;

/* @var $this yii\web\View */
$this->title = 'Round '.$round->round;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <p><?= $round->motion ?></p>
    <p>Next Round Starts at : <?= date('d M y H:i:s',strtotime($round->next->start_at)) ?></p>

    <h2>Feedback Progression: Team Submission</h2>
    <?= 
    	Progress::widget([
            'percent' => $round->progressTeam,
            'barOptions' => ['class' => 'progress-bar-success'],
            'options' => ['class' => 'active progress-striped'],
            'label' => round($round->progressTeam,2).'%',
        ])
    ?>
    <div style="overflow:hidden; background:#ddd; margin:10px 0 50px 0;">
	    <div style="width:200px; text-align:center; position:absolute; padding:10px; font-size:large; background:#555; color: white;">
	    	'Missing' Teams
	    </div>
	    <div style="width:100%; padding:10px 1%; float:left; font-size:large">
	    	<marquee>
	    		<?= $mTeams ?>
	    	</marquee>
	    </div>
    </div>


    <?= 
    	Progress::widget([
            'percent' => $round->progressAdj,
            'barOptions' => ['class' => 'progress-bar-success'],
            'options' => ['class' => 'active progress-striped'],
            'label' => round($round->progressAdj,2).'%',
        ])
    ?>
    <div style="overflow:hidden; background:#ddd; margin:10px 0 50px 0;">
	    <div style="width:200px; text-align:center; position:absolute; padding:10px; font-size:large; background:#555; color: white;">
	    	'Missing' Adjudicators
	    </div>
	    <div style="width:100%; padding:10px 1%; float:left; font-size:large">
	    	<marquee>
	    		<?= $mAdjs ?>
	    	</marquee>
	    </div>
    </div>
</div>
