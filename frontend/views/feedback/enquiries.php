<?php

use kartik\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveField;


/* @var $this yii\web\View */
/* @var $model app\models\Feedback */

$this->title = 'Aux Feedback Submission';
?>
<div class="feedback-create">

    <h1><?= Html::encode($this->title) ?></h1>

	<div class="feedback-form">
	    <?php $form = ActiveForm::begin(); ?>

	    <?= $form->field($model, "round")->textInput(['maxlength'=>2]) ?>
	    
	    <?= $form->field($model, "room")->textInput(['maxlength'=>10]) ?>

	    <?php
	        foreach($adj as $i => $a){
	            if($a->adj_id != $giver || $method == 'd2a'){
	                echo '<div id="place'.$i.'" style="margin-bottom:10px; background:#eee; padding:5px 20px;">';

	                echo '<h2>'.$a->adjudicator->name.'</h2>';
	                
	                echo $form->field($model, "[{$i}]adj_id", ['options'=>['name'=>'judges','style'=>'display:none;']])->dropdownList(ArrayHelper::map($adj, 'adj_id', 'adjNameInst'))->label('Who deliver the verbal?');
	                
	                if($method == 'a2a') echo $form->field($model, "[{$i}]agree_initial")->RadioList(['y' => 'Yes', 'n'=>'No'], ['separator'=>'&emsp;&emsp;', 'itemOptions' => ['class' =>'radio-inline', 'style'=>'margin:0 5px;']])->label('Do you agree with the initial decision of the adjudicator?');
	        
	                echo $form->field($model, "[{$i}]agree_eventual")->RadioList(['y' => 'Yes', 'n'=>'No'], ['separator'=>'&emsp;&emsp;', 'itemOptions' => ['class' =>'radio-inline', 'style'=>'margin:0 5px;']])->label('Do you agree with the result of the adjudication?');

	                foreach($question as $j => $q) echo $form->field($d_mdl, "[{$i}][{$j}]score")->radioList(array_combine(range(1,$config['max_score']),range(1,$config['max_score'])), ['separator'=>'&emsp;&emsp;', 'itemOptions' => ['class' =>'radio-inline', 'style'=>'margin:0 5px;']])->label($q->question);
	                
	                if($method == 'a2a') echo $form->field($model, "[{$i}]influences")->checkboxList(['r'=>'Result','e'=>'Explanation','s'=>'score'], ['separator'=>'&emsp;&emsp;', 'itemOptions' => ['class' =>'radio-inline', 'style'=>'margin:0 5px;']])->label('What Influences your feedback the most?');
	                else if($method == 'd2a') echo $form->field($model, "[{$i}]influences")->checkboxList(['r'=>'Result','e'=>'Explanation','f'=>'Feedback'], ['separator'=>'&emsp;&emsp;', 'itemOptions' => ['class' =>'radio-inline', 'style'=>'margin:0 5px;']])->label('What Influences your feedback the most?');

	                if($config['tagging'] == 'y') echo $form->field($model, "[{$i}]tag")->checkboxList(ArrayHelper::map($tag, 'id', 'tag'), ['separator'=>'&emsp;&emsp;', 'itemOptions' => ['class' =>'radio-inline', 'style'=>'margin:0 5px;']])->label($q->question);
	            
	                if($config['comment'] == 'y') echo $form->field($model, "[{$i}]comments")->textarea(['rows' => 6]);

	                echo '</div>';
	            }
	        }
	    ?>

	    <div class="form-group">
	        <?= Html::submitButton(Html::icon('ok').' Submit', ['class' => 'btn btn-primary']) ?>
	    </div>

	    <?php ActiveForm::end(); ?>

	</div>


</div>
