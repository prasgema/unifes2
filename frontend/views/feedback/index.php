<?php
use kartik\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Feedbacks';
$dataProvider = new ArrayDataProvider(['allModels'=>$list, 'pagination'=>false]);
?>
<script>
function addMargin() {
    window.scrollTo(0, window.pageYOffset - 100);
}

window.addEventListener('hashchange', addMargin);
</script>

<div id="links" style="position:fixed; z-index:9999; width:40px; right:0; top:0; height:100%; padding:5px; background:#ddd;">
    <?php
        foreach($list as $i=>$l)  echo Html::a($i, '#'.$i, ['class'=>'btn btn-default', 'style'=>'width:30px;margin: 1px 1px 0 0; font-size:11pt;padding:1px;']); 
    ?>
</div>


<div class="feedback-index" style="margin-right:40px;">

    <div style="text-align:center; overflow:hidden;">
        <?php $form = ActiveForm::begin(); ?>
            <?= Select2::widget([
                'name' => 'giver',
                'data' => array_merge(["" => ""], ArrayHelper::map($teams, 'team_name', 'team_name'), ArrayHelper::map($adjs,'nameInst','nameInst')),
                'options' => ['placeholder' => 'search your name / team'/*, 'style'=>'width:90%;'*/],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'addon' => [
                    'append' => [
                        'content' => Html::submitButton(Html::icon('search'), [
                            'class'=>'btn btn-primary', 
                            'style'=>'width:40px !IMPORTANT;'
                        ]),
                        'asButton'=>true,
                    ],
                    'groupOptions' => [
                        'style'=>'width:100%;'
                    ]
                ]
            ]); ?>   
        <?php ActiveForm::end(); ?>
    </div><br/>


    <div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute'=>'idx',
                'format'=>'raw',
                'value'=>function($model){
                    return '<h1 id="'.$model['idx'].'">'.$model['idx'].'</h1>';
                }
            ],
            'team:raw:Teams',
            'adjudicator:raw:Adjudicators',
        ],
        'summary'=>false,
    ]) ?>
    </div>
</div>
