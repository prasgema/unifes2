<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Feedback */

$this->title = 'Feedback Submission';
?>
<div class="feedback-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="feedback-form">
        
        <h2>
        <?php
            for($x=1; $x<=$latestRound; $x++)
            echo Html::a('R'.$x,['create', 'method'=>$method, 'giver'=>$giver, 'round'=>$x],['disabled'=> $x==$round , 'style'=>'margin:0.5%; width: '.((100/$latestRound)-1).'% ;', 'class'=> $x==$round ? 'btn btn-primary' : (in_array( ['round'=>$x], $feedback_done ) ? 'btn btn-warning' : 'btn btn-default') ])
        ?></h2><br/><br/>

        <h1>Forgetting Something?</h1>
        <p>Your feedback for this round has been submitted. Or maybe you didn't attend the round</p>
        <p>If you believe this is a mistake, please <strong style="color:red;">notify administrator</strong> AND fill your feedback here : <?= Html::a('Enquiries', ['enquiries', 'method'=>$method, 'giver'=>$giver], ['class'=>'btn btn-default']) ?></p>
        <p>Or you can go back to the <?= Html::a('Main Page', ['thankyou'],['class'=>'btn btn-default']) ?></p>

    </div>

</div>
