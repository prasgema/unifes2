<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Thank you for your feedback!';
?>
<script>
window.setTimeout(function(){
    // Move to a new location or you can do something else
    window.location.href = "<?=Url::home() ?>";
}, 2000);
</script>

<div class="feedback-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?=Html::a('[back to front page]', ['index'], ['class'=>'btn btn-primary']) ?>


</div>
