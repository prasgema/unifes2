<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\ActiveField;


/* @var $this yii\web\View */
/* @var $model app\models\Feedback */

$this->title = 'Unique Code Verification';
?>
<div class="unique-code">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="unique-code" style="margin:20px 0;">
   		<?php $form = ActiveForm::begin(); ?>

   		<h2>You are signing as <strong style="color:blue;"><?= $method == 'a2a' ? $model->name : $model->team_name ?></strong>. Enter your unique code below: </h2>
		
        <div class="input-group">
            <span class="input-group-addon" style="width:100%"><?= Html::textInput('unique-code', '',['class'=>'form-control']) ?></span>
            <span class="input-group-addon"><?= Html::submitButton('Verify!', ['class' => 'btn btn-primary']) ?></span>
        </div>       
    	<?php ActiveForm::end(); ?>
    </div>

    <?=Html::a('[back to front page]', ['index'], ['class'=>'btn btn-default']) ?>

</div>
