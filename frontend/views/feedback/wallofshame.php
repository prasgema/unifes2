<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'WALL OF SHAME';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    
    <h2>Teams</h2>
    <ul>
    <?php
        foreach($team as $t){
            if($t->missingFeedback > 0) echo '<li>'.$t->team_name.' - '.$t->missingFeedback.'</li>' ;
        }
    ?>
    </ul>

    <h2>Adjs</h2>
    <ul>
    <?php
        foreach($adj as $a){
            if($a->missingFeedback > 0) echo '<li>'.$a->name.' - '.$a->missingFeedback.'</li>' ;
        }
    ?>
    </ul>
</div>
