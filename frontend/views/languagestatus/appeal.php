<?php

use kartik\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model frontend\models\LanguageStatusAppeal */

$this->title = 'Language Status Appeal';
$this->params['breadcrumbs'][] = ['label' => 'Language Status Registration', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="language-status-appeal-create">

    <h1><?= Html::encode($this->title) ?></h1>

	<div class="language-status-appeal-form">
		<h2>Appeal for team <?= $model->team_name ?></h2>

	    <?php $form = ActiveForm::begin(); ?>

		<div style="text-align:center; overflow:hidden;">
			<?= Select2::widget([
			    'name' => 'giver',
			    'data' => array_merge(["" => ""], ArrayHelper::map($teams, 'team_name', 'team_name'), ArrayHelper::map($adjs,'nameInst','nameInst')),
			    'options' => ['placeholder' => 'Who are you?'/*, 'style'=>'width:90%;'*/],
			    'pluginOptions' => [
			        'allowClear' => true
			    ],
			    'addon' => [
			    	'prepend' => [ 'content' => 'Appealing as : ' ],
			        'append' => ['content' => 'Unique Code'],
			        'groupOptions' => ['class'=>'input-group-lg'],
			        'contentAfter' => '<input type="password" id="unique_code" name="unique_code" class="form-control" placeholder="Unique Code">',
			        'groupOptions' => [
			            'style'=>'width:100%;'
			        ]
			    ]
			]); ?>   
		</div><br/>

	    <?= $form->field($appeal, 'to_whom')->textInput(['maxlength' => true, 'placeholder' => 'which speaker(s)? If more than 1, separate with comma']) ?>

	    <?= $form->field($appeal, 'appeal')->textarea(['rows' => 6]) ?>

	    <div class="form-group">
	        <?= Html::submitButton('SUBMIT APPEAL!', ['class' => 'btn btn-primary']) ?>
	    </div>

	    <?php ActiveForm::end(); ?>

	</div>

</div>