<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\LanguageStatus */

$this->title = 'Apply Language Status for Team '.$model->team_name;
$this->params['breadcrumbs'][] = ['label' => 'Front Page', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="language-status-create">

    <h1><?= Html::encode($this->title) ?></h1>

	<div class="language-status-form">

	    <?php $form = ActiveForm::begin(); ?>

	    <h2>#1 : <?= $model->debater_1 ?></h2>
	    <?= $form->field($langstats, 'speaker_1_applying_as')->dropdownList($statuses, ['prompt'=>'NORMAL']) ?>

	    <?php foreach($questions as $i=>$qa){ 
	    	list($q, $a) = explode(':', $qa);
	    	$a = explode(',', $a);
	    ?>
	   		<?= $form->field($langstats, "speaker_1_interview[{$i}]")->dropdownList($a)->label($q) ?>
	    <?php } ?>

	    <hr/><h2>#2 : <?= $model->debater_2 ?></h2>
	    <?= $form->field($langstats, 'speaker_2_applying_as')->dropdownList($statuses, ['prompt'=>'NORMAL']) ?>

	    <?php foreach($questions as $i=>$qa){ 
	    	list($q, $a) = explode(':', $qa);
	    	$a = explode(',', $a);
	    ?>
	   		<?= $form->field($langstats, "speaker_2_interview[{$i}]")->dropdownList($a)->label($q) ?>
	    <?php } ?>

	    <?php if($model->debater_3 != ''){ ?>
		    <hr/><h2>#3 : <?= $model->debater_3 ?></h2>
	    	<?= $form->field($langstats, 'speaker_3_applying_as')->dropdownList($statuses, ['prompt'=>'NORMAL']) ?>

		    <?php foreach($questions as $i=>$qa){ 
		    	list($q, $a) = explode(':', $qa);
		    	$a = explode(',', $a);
		    ?>
		   		<?= $form->field($langstats, "speaker_3_interview[{$i}]")->dropdownList($a)->label($q) ?>
		    <?php } ?>
	    <?php } ?>

	    <div class="form-group">
	        <?= Html::submitButton('APPLY!', ['class' => 'btn btn-success']) ?>
	    </div>

	    <?php ActiveForm::end(); ?>

	</div>

</div>