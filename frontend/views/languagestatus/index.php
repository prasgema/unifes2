<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Language Status Registration';
?>
<div class="tag-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'emptyText' => '',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'team_name',
            'debater_1',
            'languageStatus.d1s:raw',
            'debater_2',
            'languageStatus.d2s:raw',
            'debater_3',
            'languageStatus.d3s:raw',

            [
            	'label'=>'',
            	'format'=>'raw',
            	'value'=>function($model){
            		return '<div class="btn-group">'.
            			Html::a('Apply', ['languagestatus/apply', 'id'=>$model->id], ['class'=>'btn btn-success', 'disabled'=>($model->languageStatus != null)]).
            			Html::a('Appeal', ['languagestatus/appeal', 'id'=>$model->id], ['class'=>'btn btn-warning']).
            			'</div>';
            	}
            ],
        ],
    ]); ?>

</div>