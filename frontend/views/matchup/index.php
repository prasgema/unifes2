<?php
/* @var $this yii\web\View */
use kartik\helpers\Html;
use yii\grid\GridView;

$columns = [];
$columns[] = 'debate.room';
if(Yii::$app->params['config']['system'] == 'tabbie'){
	$columns[] = 'debate.teamOG.team_name:text:Opening Gov';
	$columns[] = 'debate.teamOO.team_name:text:Opening Opp';
    $columns[] = 'debate.teamCG.team_name:text:Closing Gov';
    $columns[] = 'debate.teamCO.team_name:text:Closing Opp';
} else {
	$columns[] = 'debate.teamOG.team_name:text:Gov';
	$columns[] = 'debate.teamOO.team_name:text:Opp';
}
$columns[] = [
    'attribute'=>'status',
    'format'=>'raw',
    'value'=>function($model){
        return $model->adjudicator->nameInst.' '.($model->status=='c' ? Html::bsLabel('Chair', Html::TYPE_SUCCESS) : 
            ($model->status=='p' ? Html::bsLabel('Panel', Html::TYPE_INFO) : 
                ($model->status=='t' ? Html::bsLabel('Trainee', Html::TYPE_WARNING) : 
                    Html::bsLabel('Unidentified', Html::TYPE_DANGER))));
    }
];
$this->registerJs('
    var gridview_id = ""; // specific gridview
    var columns = [1,2,3,4,5,6,7]; // index column that will grouping, start 1

    var column_data = [];
        column_start = [];
        rowspan = [];

    for (var i = 0; i < columns.length; i++) {
        column = columns[i];
        column_data[column] = "";
        column_start[column] = null;
        rowspan[column] = 1;
    }

    var row = 1;
    $(gridview_id+" table > tbody  > tr").each(function() {
        var col = 1;
        $(this).find("td").each(function(){
            for (var i = 0; i < columns.length; i++) {
                if(col==columns[i]){
                    if(column_data[columns[i]] == $(this).html()){
                        $(this).remove();
                        rowspan[columns[i]]++;
                        $(column_start[columns[i]]).attr("rowspan",rowspan[columns[i]]);
                    }
                    else{
                        column_data[columns[i]] = $(this).html();
                        rowspan[columns[i]] = 1;
                        column_start[columns[i]] = $(this);
                    }
                }
            }
            col++;
        })
        row++;
    });
');
?>

<h1>Matchup</h1>

<div class="btn-group">
<?php
	for($x=1;$x<=$latestround;$x++){
		echo Html::a('R'.$x, ['index', 'round'=>$x], ['class'=> $x==$round?'btn btn-primary':'btn btn-default']);
	}
?>
</div>

<?= GridView::widget([
        'dataProvider' => $matchup,
        'tableOptions'=>['class'=>'table table-striped', 'style'=>'font-size:small;'],
        'columns' => $columns,
    ]);
    ?>