<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
?>
<h1>Adjudicator Re-Registration</h1>

<div class="adjudicator-update">

    <h1><?= Html::encode($this->title) ?></h1>

	<div class="adjudicator-form">

	    <?php $form = ActiveForm::begin(); ?>

	    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

	    <?= $form->field($model, 'institution')->textInput(['maxlength' => true]) ?>

	    <div class="form-group">
	        <?= Html::submitButton('CONFIRM!', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    	<?= Html::a('Back to Front Page', ['rego/index'], ['class'=>'btn btn-default']) ?>
	    </div>

	    <?php ActiveForm::end(); ?>

	</div>

</div>
