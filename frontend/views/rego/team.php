<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
?>
<h1>Team Re-Registration</h1>

<div class="adjudicator-update">

	<div class="team-form">

	    <?php $form = ActiveForm::begin(); ?>

	    <?= $form->field($model, 'team_name')->textInput(['maxlength' => true]) ?>

	    <?= $form->field($model, 'debater_1')->textInput(['maxlength' => true]) ?>

	    <?= $form->field($model, 'debater_2')->textInput(['maxlength' => true]) ?>

	    <?= $form->field($model, 'debater_3')->textInput(['maxlength' => true])->label('Debater 3 (Ignore on British Parliamentary Debates)') ?>

	    <div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	        <?= Html::a('Back to Front Page', ['rego/index'], ['class'=>'btn btn-default']) ?>
	    </div>

	    <?php ActiveForm::end(); ?>

	</div>
</div>
